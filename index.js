const express = require('express');
const mongoose = require('mongoose');
const cookieSession = require('cookie-session');
const passport = require('passport');
bodyParser = require('body-parser');
const keys = require('./config/keys');
const cors = require ('cors');
const morgan = require('morgan');
require('./models/User');
require('./models/Drive');
require('./models/Site');
require('./services/passport');

mongoose.Promise = global.Promise;
mongoose.connect(keys.mongoURI, {
  useMongoClient: true
});

const app = express();

//middlewares
app.use(morgan('combined'));
app.use(bodyParser.json());
app.use(
    cookieSession({
        // 30 days * 24 hours in a day * 60 minutes per hour * 60 seconds in an hour * 1000 ms in a second
        maxAge: 30 * 24 * 60 * 60 * 1000,
        keys: [keys.cookieKey] //key to encrypt cookie
    })
);
app.use(passport.initialize());
app.use(passport.session());
app.use(cors());

//return function with an immidate call to app
require('./routes/authRoutes')(app);
require('./routes/driveRoutes')(app);
require('./routes/siteRoutes')(app);

if (process.env.NODE_ENV === 'production') {
  // Express will serve up production assets
  // like our main.js file, or main.css file!
  app.use(express.static('client/build'));

  // Express will serve up the index.html file
  // if it doesn't recognize the route
  const path = require('path');
  app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
  });
}

//assign port variable on prod, 5000 on dev
const PORT = process.env.PORT || 5000;
app.listen(PORT);
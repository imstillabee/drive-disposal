const mongoose = require('mongoose');
const { Schema } = mongoose;
const DriveSchema = require('./Drive');

const siteSchema = new Schema({
  name: String,
  region: { type: String, default: null },
  personnel: { type: String, default: null },
  phone: {type: String, default: null },
  mobile: {type: String, default: null },
  hours: {type: String, default: null },
  email: {type: String, default: null },
  image: {type: String, default: null},
  drives: [DriveSchema]
});

mongoose.model('sites', siteSchema);
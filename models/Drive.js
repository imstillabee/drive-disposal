const mongoose = require('mongoose');
const { Schema } = mongoose;
const RecipientSchema = require('./Recipient');

const driveSchema = new Schema({
  serialNumber: String,
  location: String,
  driveType: String,
  manufacturer: String,
  vendorPickup: { type: String, default: null },
  cod: { type: String, default: null },
  recipients: [RecipientSchema],
  yes: { type: Number, default: 0 },
  no: { type: Number, default: 0},
  _user: { type: Schema.Types.ObjectId, ref: 'User'},
  depositDate: Date,
  lastResponded: Date,
  price: { type: Number, default: 0 }
});

driveSchema.pre('save', function(next) {
  let self = this;

  if (self.manufacturer === 'Dell') {
    self.price = 45;
    next();
  } else if (self.manufacturer === 'HP') {
    self.price = 55;
    next();
  } else if (self.manufacturer === 'Toshiba') {
    self.price === 35;
    next();
  } else if ( self.manufacturer === 'Seagate') {
    self.price === 60;
    next();
  } else if (self.manufacturer === 'Cisco') {
    self.price === 90;
    next();
  } else {
    next();
  }  
});

mongoose.model('drives', driveSchema);
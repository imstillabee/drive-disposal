FROM node:latest

LABEL version="1.0"
LABEL description="Drive Disposal Image"

# Create app directory
RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app
# Install app dependencies
COPY package.json /usr/src/app/package.json
RUN npm install

# Bundle app source
COPY config /usr/src/app/config
COPY middlewares /usr/src/app/middlewares
COPY models /usr/src/app/models
COPY routes /usr/src/app/routes
COPY services /usr/src/app/services
COPY index.js /usr/src/app
COPY sendgrid_webhook.sh /usr/src/app
COPY client /usr/src/app/client
RUN cd /usr/src/app/client && npm install && npm run build

EXPOSE 3001
ENV MONGODB_URI=mongodb://mongo/drivedisposal

CMD ["npm", "run-script", "dev"]
const mongoose = require('mongoose');
const passportService = require('../services/passport');
const passport = require('passport');

const requireAuth = passport.authenticate('jwt', {session: false});
const Site = mongoose.model('sites');

module.exports = app => {
     app.get('/api/sites', async (req, res) => {
        const sites = await Site.find().sort({name: 1});

        res.send(sites);
    });

    app.get('/api/sites/:location', async (req, res) => {
        const sites = await Site.find({ name: req.params.location }).sort({name: 1, dateSent: -1})

        res.send(sites);
    });


    app.post('/api/site', async (req, res) => {
        const site = new Site(req.body);
        
        site.save(function(err) {
            if(err)
                res.send(err);

            res.json({message: 'Site Created!' });
        });
    });

    app.delete('/api/sites/delete/:id', async (req, res) => {
        await Site.deleteOne({ _id: req.params.id });
        const sites = await Site.sort({name: 1});
        res.send(sites);
    });


    app.put('/api/site/:id', async (req, res) => {
        const site = await Site.update({ _id: req.params.id}, req.body, function(err) {
            if(err)
                res.send(err);

            res.json({message: 'Site Updated!"'})
        })
    })

    app.post('/api/sites', async (req, res) => {
        Site.insertMany(req.body.site_names, function(err, sites) {
            if(err)
                res.send(err);

            res.json({message: 'Sites Created!' });
        });
    });
};
const _ = require('lodash');
const Path = require('path-parser');
const { URL } = require('url');
const mongoose = require('mongoose');
const passportService = require('../services/passport');
const passport = require('passport');
const nodeMailer = require('nodemailer');
const config = require('../config/keys');

const requireAuth = passport.authenticate('jwt', {session: false}); 

const Drive = mongoose.model('drives');

module.exports = app => {

    app.get('/api/drives', async (req, res) => {
        const drives = await Drive.find().sort({location: 1, dateSent: -1});

        res.send(drives);
    });

    app.get('/api/drives/:location', async (req, res) => {
        const drives = await Drive.find({ location: req.params.location }).sort({location: 1, dateSent: -1});

        res.send(drives);
    });

     app.get('/api/drives/:driveId/:choice', async (req, res) => {
         const choice = req.params.choice;
         await Drive.updateOne({ "_id": req.params.driveId}, { $inc: { [choice]: 1}, "lastResponded": new Date()});
         
         const drive = await Drive.find({ "_id": req.params.driveId });

         let transporter = nodeMailer.createTransport({
            host: '98.132.135.94',
            port: 25,
            tls: {
                rejectUnauthorized: false
            },
            debug: true
        });

        let mailOptions = {
          from: '"no-reply" <xx@verizonwireless.com>', // sender address
          to: drive.recipients[0].email, // list of receivers
          subject: "Disposed Drive Verified", // Subject line
          text: req.body.body, // plain text body
          html: `
        <html>
            <body>
                <div style="text-align: center;">
                    <h3>Drive Disposal Verification</h3>
                    <p>Thank You! Drive Disposal has been updated with your verification for <b>${drive.serialNumber}</b></p>
                </div>
            </body>
        </html>`
        };

        const mailer = transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                return console.log(error);
            }
            console.log('Message %s sent: %s', info.messageId, info.response);
                res.render('index');
            });         
        
         res.send('Thank You, your verification has been sent back to the original poster')
    });

    app.delete('/api/drives/delete/:id', async (req, res) => {
        await Drive.deleteOne({ _id: req.params.id });
        // const drives = await Drive.find({ _user: req.user.id }).sort({location: 1, dateSent: -1});
        const drives = await Drive.find({ _user: req.user.id }).sort({location: 1, dateSent: -1});
        res.send(drives);
    });

    app.post('/api/drives/webhooks', (req, res) => {
        const p = new Path('/api/drives/:driveId/:choice');

        _.chain(req.body)
            .map(({ url }) => {
                const match = p.test(new URL(url).pathname);
                if (match) {
                    return { driveId: match.driveId, choice: match.choice };
                }
            })
            .compact()
            .uniqBy('driveId')
            .each(({ driveId, choice }) => {
                Drive.updateOne(
                    {
                        _id: driveId
                    },
                    {
                        $inc: { [choice]: 1 },
                        lastResponded: new Date()
                    }
                ).exec();
            })
            .value();

        res.send({});

    });

    app.post('/api/drives', async (req, res) => {
        const { serialNumber, location, driveType, manufacturer, recipients} = req.body;

        const drive = new Drive({
            serialNumber,
            location,
            driveType,
            manufacturer,
            recipients: recipients.split(',').map(email => ({ email: email.trim() })),
            //user,
            //_user: req.user._id,
            depositDate: Date.now().toLocaleString() 
        });


        let transporter = nodeMailer.createTransport({
            host: '98.132.135.94',
            port: 25,
            tls: {
                rejectUnauthorized: false
            },
            debug: true
        });

        let mailOptions = {
          from: '"no-reply" <xx@verizonwireless.com>', // sender address
          to: drive.recipients[0].email, // list of receivers
          subject: "Verify Disposed Drive", // Subject line
          text: req.body.body, // plain text body
          html: `
        <html>
            <body>
                <div style="text-align: center;">
                    <h3>Drive Disposal Verification</h3>
                    <p>${drive.recipients[0].email}, you have been requested to verify if the following drive <b>${drive.serialNumber}</b> is properly disposed at the follwing site <b>${drive.location}</b> at the time of ${drive.depositDate}. Please select an option below to confirm.</p>
                    <div>
                        <a href="${config.redirectDomain}/api/drives/${drive.id}/yes">Vaild</a>
                    </div>
                    <div>
                        <a href="${config.redirectDomain}/api/drives/${drive.id}/no">Invalid</a>
                    </div>
                </div>
            </body>
        </html>`
        };

        const mailer = transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                return console.log(error);
            }
            console.log('Message %s sent: %s', info.messageId, info.response);
                res.render('index');
            });

        // Send confirmation email to reps
        // const mailer = new Mailer(drive, driveTemplate(drive));
        try {
            await mailer.send();
            await drive.save();
            const user = await req.user.save();

            res.send(user);
         } catch (err) {
            res.status(422).send(err); //422 - unprocessible entity
        }

    });


    app.post('/api/manydrives', async (req, res) => {
        Drive.insertMany(req.body.drive_names, function(err, drives) {
            if(err)
                res.send(err);

            res.json({message: 'Drives Created!' });
        });
    });
}
//key.js is used for prod env

if (process.env.NODE_ENV === 'production') {
    module.exports = require('./prod');
} else if (process.env.NODE_ENV === 'stage') {
    module.exports = require('./stage');
} else {
    module.exports = require('./dev');
}
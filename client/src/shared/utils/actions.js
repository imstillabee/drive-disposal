export const setWindowResize = size => {
  return {
    type: 'WINDOW_RESIZE',
    windowSize: size
  }
}

export const setWindowScroll = position => {
  return {
    type: 'WINDOW_SCROLL',
    windowScroll: position
  }
}

export const setCurrentRoute = route => {
  return {
    type: 'ROUTE_CHANGE',
    currentRoute: route
  }
}

export const updateRouteTitle = title => {
  return {
    type: 'UPDATE_ROUTE_TITLE',
    title: title
  }
}

export default (state = [], action) => {
  switch (action.type) {
    case 'WINDOW_RESIZE':
      return Object.assign({}, state, {
        ...state,
        windowSize: action.windowSize
      })
    case 'WINDOW_SCROLL':
      return Object.assign({}, state, {
        ...state,
        windowScroll: action.windowScroll
      })
    case 'ROUTE_CHANGE':
      return Object.assign({}, state, {
        ...state,
        currentRoute: action.currentRoute
      })
    case 'UPDATE_ROUTE_TITLE':
      return Object.assign({}, state, {
        ...state,
        currentRoute: {
          ...state.currentRoute,
          title: action.title
        }
      })
    default:
      return state
  }
}

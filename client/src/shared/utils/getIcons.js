let fileList = require.context('_icons/small', true, /[\s\S]*$/)

let dictionary = {}

fileList.keys().forEach(x => {
  x = x.replace('./', '')
  dictionary[x.replace('.svg', '')] = require(`_icons/small/${x}`)
})

export default dictionary
export const shadow = {
  0: '0 0 0 0 transparent',
  1: '0  1px  2px 0 rgba(0,0,0,0.2), 0 0 1px 0 rgba(0,0,0,0.15)',
  2: '0  2px  4px 0 rgba(0,0,0,0.2), 0 0 4px 0 rgba(0,0,0,0.15)',
  3: '0  4px  8px 0 rgba(0,0,0,0.2), 0 0 4px 0 rgba(0,0,0,0.15)',
  4: '0  8px 12px 0 rgba(0,0,0,0.2), 0 0 4px 0 rgba(0,0,0,0.15)',
  5: '0 12px 16px 0 rgba(0,0,0,0.2), 0 0 4px 0 rgba(0,0,0,0.15)',
  6: '0 16px 24px 0 rgba(0,0,0,0.2), 0 0 4px 0 rgba(0,0,0,0.15)',
  7: '0 24px 32px 0 rgba(0,0,0,0.2), 0 0 4px 0 rgba(0,0,0,0.15)',
  8: '0 32px 48px 0 rgba(0,0,0,0.2), 0 0 4px 0 rgba(0,0,0,0.15)'
}

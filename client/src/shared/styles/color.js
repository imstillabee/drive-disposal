export const red = {
  100: '#FF8A81',
  200: '#FF5D50',
  300: '#F5493D',
  400: '#E0372A',
  500: '#D52B1E',
  600: '#C42317',
  700: '#B51C10',
  800: '#B51C10',
  900: '#B51C10'
}

export const orange = {
  100: '#FFC69C',
  200: '#FFAD70',
  300: '#FF993E',
  400: '#F78016',
  500: '#ED7000',
  600: '#E06003',
  700: '#D35903',
  800: '#BE4F00',
  900: '#A64400'
}

export const amber = {
  100: '#FFD78A',
  200: '#FFC963',
  300: '#FFBC3D',
  400: '#FFB220',
  500: '#FFA700',
  600: '#F39800',
  700: '#DF8A05',
  800: '#CC7B02',
  900: '#B26B00'
}

export const yellow = {
  100: '#FEF56C',
  200: '#FDF145',
  300: '#FFEA2B',
  400: '#FFE21B',
  500: '#FFD500',
  600: '#F7C700',
  700: '#ECB80B',
  800: '#E2AA00',
  900: '#D9A306'
}

export const lime = {
  100: '#CCF57A',
  200: '#C0F25C',
  300: '#AFEB38',
  400: '#9FE515',
  500: '#97DC10',
  600: '#8DCF0A',
  700: '#83BC13',
  800: '#78A919',
  900: '#6D9C11'
}

export const green = {
  100: '#76EAA1',
  200: '#57E28A',
  300: '#2DD269',
  400: '#0ABB4A',
  500: '#00AC3E',
  600: '#009636',
  700: '#018631',
  800: '#03742C',
  900: '#016726'
}

export const turquoise = {
  100: '#89F4D7',
  200: '#64ECC8',
  300: '#37E8B8',
  400: '#22DBA9',
  500: '#12CE9B',
  600: '#11B78A',
  700: '#0BA078',
  800: '#078C68',
  900: '#047C5B'
}

export const sky = {
  100: '#ADE8FC',
  200: '#8CE2FF',
  300: '#65D8FF',
  400: '#3BCDFF',
  500: '#00BDFF',
  600: '#06A4EE',
  700: '#0091D5',
  800: '#0081C4',
  900: '#0072AD'
}

export const blue = {
  100: '#7CB9FF',
  200: '#53A4FF',
  300: '#3694FF',
  400: '#1A85FF',
  500: '#0077FF',
  600: '#056CE2',
  700: '#025FCA',
  800: '#0552AA',
  900: '#034795'
}

export const purple = {
  100: '#C596FA',
  200: '#AD67FD',
  300: '#993FFF',
  400: '#8723F8',
  500: '#7A13EF',
  600: '#6D12D3',
  700: '#6116B6',
  800: '#5914A6',
  900: '#4B0D91'
}

export const pink = {
  100: '#FFA4E6',
  200: '#FF7FD8',
  300: '#FE57CB',
  400: '#FC3EC2',
  500: '#F329B5',
  600: '#DF16A1',
  700: '#CB078F',
  800: '#B50880',
  900: '#A30573'
}

export const fuchsia = {
  100: '#FF8AB2',
  200: '#FF6499',
  300: '#FF508C',
  400: '#F9397B',
  500: '#F3296E',
  600: '#E42566',
  700: '#D61C5B',
  800: '#C21953',
  900: '#AC1146'
}

export const gray = {
  50: '#F6F6F6',
  100: '#EFF0EE',
  200: '#E4E5E3',
  300: '#D8DADA',
  400: '#CCCCCC',
  500: '#ADADAD',
  600: '#959595',
  700: '#5C5C5C',
  800: '#333333',
  900: '#222222'
}

export const white = '#FFFFFF'
export const black = '#000000'

export const hexToRgb = hex => {
  var result = (/^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i).exec(hex)

  return result ? {
    r: parseInt(result[1], 16),
    g: parseInt(result[2], 16),
    b: parseInt(result[3], 16)
  } : null
}

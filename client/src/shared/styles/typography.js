import { gray, red } from './color'

export const truncate = {
  whiteSpace: 'nowrap',
  overflow: 'hidden',
  textOverflow: 'ellipsis'
}

export const h1 = {
  fontFamily: '\'Neue Haas Grotesk Display Std\'',
  fontSize: '48px',
  fontWeight: 'bold',
  color: 'black'
}

export const h2 = {
  fontFamily: '\'Neue Haas Grotesk Display Std\'',
  fontSize: '32px',
  fontWeight: 'bold',
  color: red[500]
}

export const h3 = {
  fontFamily: '\'Neue Haas Grotesk Display Std\'',
  fontSize: '24px',
  fontWeight: 'bold',
  color: gray[900]
}

export const h4 = {
  fontFamily: '\'Neue Haas Grotesk Display Std\'',
  fontSize: '20px',
  fontWeight: 'bold',
  color: gray[900]
}

export const h5 = {
  fontFamily: '\'Neue Haas Grotesk Display Std\'',
  fontSize: '16px',
  fontWeight: 'bold',
  textTransform: 'uppercase',
  color: gray[600]
}

export const h6 = {
  fontFamily: '\'Neue Haas Grotesk Display Std\'',
  fontSize: '16px',
  fontWeight: 'bold',
  color: gray[900]
}

export const body = {
  fontFamily: '\'Neue Haas Grotesk Text Std\'',
  fontSize: '16px',
  lineHeight: '1.5',
  fontWeight: 'regular',
  color: gray[700]
}

export const label = {
  fontFamily: '\'Neue Haas Grotesk Text Std\'',
  fontSize: '12px',
  fontWeight: 'bold',
  color: gray[600]
}

export const anchor = {
  color: red[500],
  transition: '200ms',
  ':hover': {
    color: red[300]
  }
}

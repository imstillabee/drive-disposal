import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import AppBar from './AppBar'
import Dialog from './Dialog'
import { gray } from '../../shared/styles/color'
import Button from './Button'
import { ContextMenu, ContextMenuContent, ContextMenuItem, ContextMenuDivider } from './ContextMenu'
import { fetchHost, fetchUser } from '../../actions';
import appConfig from '../../app.config'
class AppAppBar extends Component {
  constructor (props) {
    super(props)
    this.state = { 
      matches: [], 
      disclaimerDialogOpen: false,
      userLocations: '',
      userName: '',
      userRole: '',
      currentRoute: props.currentRoute
     }
    this.props.fetchUser();
  }

  componentDidMount() {
        setTimeout(function() { this.setUser(); }.bind(this), 6000)
    }

    handleUpdateSearch = (query) => {
    if(query && query !== '') {
      this.props.fetchHost(query)
      .then(matches => {
        console.log(this.props.host)
        this.setState({ matches: this.props.host.map(match => { return {
          text: match.hostname ? match.hostname : 'No hostname assigned',
          subtext: match.sn,
          link: `https://dev.moss.verizoncloudplatform.com/host/${match.hostname ? match.hostname.toLowerCase() : '_'}/${match.sn}`,
          icon: 'server'
        } }) })
      })
      .catch(err => {
        console.info('error')
        console.error(err)
      })
    } else {
      this.setState({ matches: [] })
    }
  }


  handleOpenDisclaimer = () => {
    this.setState({disclaimerDialogOpen: true});
  }

  handleCloseDisclaimer = () => {
    this.setState({disclaimerDialogOpen: false});
  }

  setUser() {
        this.setState((state) => ({
            userLocations: this.props.user.profile.role && this.props.user.profile.locations,
            userName: this.props.user && this.props.user.display_name,
            userRole: this.props.user.profile && this.props.user.profile.role
        }))
        console.log(this.props.user )
  }

  renderBar ({ props, props: { style, scrollTop, auth } } = this) {
    const { currentRoute } = this.state
    const userLocations = this.state.userLocations
    const userName = this.state.userName
    const userRole = this.state.userRole

    const disclaimerDialogActions = (
      <div style={{textAlign: 'right'}}>
        <Button type="primary" onClick={this.handleCloseDisclaimer}>Okay</Button>
      </div>
    )

    const appActions = (
      <div>
        <ContextMenu
          menuOrigin={{ horizontal: 'right', vertical: 'top' }}
          triggerOrigin={{ horizontal: 'right', vertical: 'bottom' }}>
          <Button type='icon' icon='more-vertical' onClick={ () => { } } />
          <ContextMenuContent>
            { userName && (
              <div style={{ padding: '8px 16px' }}>
                <div style={{color: gray[500], fontWeight: 'bold', fontSize: 13}}>Logged in as</div>
                <div>{ userName }</div>
              </div>
            )}
            { userRole && (
              <div style={{ padding: '8px 16px' }}>
                <div style={{color: gray[500], fontWeight: 'bold', fontSize: 13}}>Role</div>
                <div>{ userRole }</div>
              </div>
            )}
            { userLocations && (
              <div style={{ padding: '8px 16px' }}>
                <div style={{color: gray[500], fontWeight: 'bold', fontSize: 13}}>Site{userLocations.length > 1 && 's'}</div>
                { userLocations.map((location, i) => (
                  <div key={'user'+i}>{`${location.city}, ${location.state}`}</div>
                )) }
              </div>
            ) }
            { (userName || userRole || userLocations) && ( <ContextMenuDivider /> ) }
            <ContextMenuItem
              label='Legal Disclaimer'
              icon='page-text'
              onClick={ this.handleOpenDisclaimer } />
            <Link to='/dd/logout' style={{ textDecoration: 'none' }}>
              <ContextMenuItem
                label='Logout'
                icon='user'
                onClick={ () => { props.logoutUser() } } />
            </Link>
          </ContextMenuContent>
        </ContextMenu>
        <Dialog
          title="Legal disclaimer"
          open={ this.state.disclaimerDialogOpen }
          actions={ disclaimerDialogActions }>
          All content is VZ confidential and proprietary. Use, disclosure, or distribution of this material is not permitted to any unauthorized persons or third parties except by written agreement. This also applies to any copies you might print.
        </Dialog>
      </div>
    )

    return (
      <AppBar
        title={ currentRoute }
        hasTabs={ currentRoute && currentRoute.hasTabs }
        hideSearchBar={ currentRoute && currentRoute.hideSearchBar }
        hideTitleBar={ currentRoute && currentRoute.hideTitleBar }
        collapsed={ currentRoute && currentRoute.appBarCollapsed }
        transparent={ currentRoute && currentRoute.appBarTransparent }
        scrollTop={ scrollTop && scrollTop.y }
        searchBar={{
          placeholder: 'Search hostnames',
          matches: this.state.matches,
          onChange: this.handleUpdateSearch
        }}
        pages={ appConfig.modules.filter((module, index) => module.showInNav ) }
        appActions={ appActions && appActions }
       
        style={{ ...style }}
      />
    )
  }

  render () {
    return(
      <div>
      {this.renderBar()}
      </div>
    )
  }
}

const mapStateToProps = ({ auth, user, host, props }) => ({ auth, user, host, props });

export default connect(mapStateToProps, {fetchHost, fetchUser})(AppAppBar);

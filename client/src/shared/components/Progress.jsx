import React, { Component } from 'react'
import Radium from 'radium'
import { red } from '../styles/color'

@Radium
export default class Progress extends Component {
  render() {
    const rotateKeyframes = Radium.keyframes({
      '100%': {transform: 'rotate(360deg)'},
    }, 'rotate')
    const dashKeyframes = Radium.keyframes({
      '0%': {
        strokeDasharray: '1, 200',
        strokeDashoffset: '0'
      },
      '50%': {
        strokeDasharray: '89, 200',
        strokeDashoffset: '-21px'
      },
      '100%': {
        strokeDasharray: '89, 200',
        strokeDashoffset: '-124px'
      },
    }, 'dash')
    return (
      <div style={{
        width: '80px',
        height: '80px',
        position: 'relative'
      }}>
        <svg viewBox="25 25 50 50" style={{
          animation: 'x 1.33s linear infinite',
          animationName: rotateKeyframes,
          height: '100%',
          transformOrigin: 'center center',
          width: '100%',
          position: 'absolute',
          top: 0,
          bottom: 0,
          left: 0,
          right: 0
        }}>
          <circle cx="50" cy="50" r="20" fill="none" strokeWidth="3" strokeMiterlimit="10" style={{
            strokeDasharray: '1, 200',
            strokeDashoffset: '0',
            animation: 'x 1.5s ease-in-out infinite',
            animationName: dashKeyframes,
            stroke: red[500]
          }} />
        </svg>
      </div>
    )
  }
}

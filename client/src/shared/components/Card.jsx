import React, { Component } from 'react'
import Radium from 'radium'
import {shadow} from '../styles/shadow'

@Radium
export default class Card extends Component {
  render({props: { children, style, level, hoverLevel, theme }} = this) {
    const backgroundColor = (() => {
      switch (theme) {
        case "white": return "#FFFFFF"
        case "light": return "#F6F6F6"
        case "dark":  return "#333333"
        default:      return "#FFFFFF"
      }
    })()
    const card = {
      width: '100%',
      borderRadius: 2,
      backgroundColor,
      boxShadow: level ? shadow[parseInt(level, 10)] : shadow[2],
      transition: '200ms'
    }
    const hoverStyles = hoverLevel ? {
      ':hover': {
        boxShadow: shadow[parseInt(hoverLevel, 10)]
      }
    } : null
    return (
      <div style={{...card, ...hoverStyles, ...style}}>{children}</div>
    )
  }
}

import React, { Component } from 'react'
import Radium from 'radium'
import Icon from './Icon'
import { red, gray, sky } from '../styles/color'
import { hexToRgb } from '../styles/color'
import { truncate } from '../styles/typography'

@Radium
export default class Button extends Component {

  render({ props: {children, style, className, onClick, size, text, type, theme, disabled, icon, submit, iconPosition} } = this) {

    const borderWidth = 2

    const defaultStyles = {
      borderWidth,
      borderStyle: 'solid',
      borderRadius: 128,
      boxSizing: 'border-box',
      cursor: 'pointer',
      fontWeight: 'bold',
      transition: '.2s',
      outline: 'none',
      display: type !== 'icon' && 'inline-flex',
      verticalAlign: 'top',
      alignItems: 'center',
      justifyContent: 'center',
      fontFamily: '"Neue Haas Grotesk Text Std", sans-serif'
    }

    const sizeStyles = (() => {
      switch(size) {
        case "xs": return {
          fontSize: 12,
          height: 24,
          lineHeight: 24 - (borderWidth*2) + 'px',
          paddingLeft: 12,
          paddingRight: 12
        }
        case "sm": return {
          fontSize: 14,
          height: 32,
          lineHeight: 32 - (borderWidth*2) + 'px',
          paddingLeft: 12,
          paddingRight: 12
        }
        case "md": return {
          fontSize: 15,
          height: 40,
          lineHeight: 40 - (borderWidth*2) + 'px',
          paddingLeft: 16,
          paddingRight: 16
        }
        case "lg": return {
          fontSize: 16,
          height: 48,
          lineHeight: 48 - (borderWidth*2) + 'px',
          paddingLeft: 24,
          paddingRight: 24
        }
        case "xl": return {
          fontSize: 20,
          height: 64,
          lineHeight: 64 - (borderWidth*2) + 'px',
          paddingLeft: 32,
          paddingRight: 32
        }
        default: return {
          fontSize: 15,
          height: 40,
          lineHeight: 40 - (borderWidth*2) + 'px',
          paddingLeft: 16,
          paddingRight: 16
        }
      }
    })()

    const typeStyles = (() => {

      if(theme === 'light') {
        return {
          color: gray[900],
          backgroundColor: 'white',
          borderColor: 'white',
          ':hover': {
            backgroundColor: 'white',
            borderColor: 'white',
            boxShadow: '0 4px 8px 0 rgba(0,0,0, 0.2)',
            color: red[500]
          },
          ':focus': {
            boxShadow: 'inset 0 0 0 2px ' + gray[900]
          },
          ':active': {
            backgroundColor: gray[100],
            borderColor: gray[100],
            boxShadow: 'inset 0 0 0 0 transparent'
          }
        }
      }

      switch(type) {
        case "primary": return {
          color: 'white',
          backgroundColor: red[500],
          borderColor: red[500],
          ':hover': {
            backgroundColor: red[400],
            borderColor: red[400],
            boxShadow: '0 4px 8px 0 rgba(' + hexToRgb(red[500]).r + ', ' + hexToRgb(red[500]).g + ', ' + hexToRgb(red[500]).b + ', 0.33)'
          },
          ':focus': {
            boxShadow: 'inset 0 0 0 2px white'
          },
          ':active': {
            backgroundColor: red[600],
            borderColor: red[600],
            boxShadow: 'inset 0 0 0 0 transparent'
          }
        }
        case "secondary": return {
          color: red[500],
          backgroundColor: gray[100],
          borderColor: gray[100],
          ':hover': {
            color: red[400],
            backgroundColor: gray[50],
            borderColor: gray[50],
            boxShadow: '0 4px 8px 0 rgba(0,0,0,0.2)'
          },
          ':focus': {
            boxShadow: `inset 0 0 0 2px ${red[500]}`
          },
          ':active': {
            backgroundColor: gray[200],
            borderColor: gray[200],
            boxShadow: 'inset 0 0 0 0 transparent'
          }
        }
        case "outlined": return {
          color: red[500],
          backgroundColor: 'transparent',
          borderColor: red[500],
          ':hover': {
            backgroundColor: `rgba(${hexToRgb(red[500]).r}, ${hexToRgb(red[500]).g}, ${hexToRgb(red[500]).b}, 0.1)`
          },
          ':focus': {
            boxShadow: 'inset 0 0 0 1px ' + red[500]
          },
          ':active': {
            color: 'white',
            backgroundColor: red[500]
          }
        }
        case "icon": return {
          color: red[500],
          backgroundColor: 'transparent',
          borderColor: 'transparent',
          borderWidth: 0,
          width: 40,
          height: 40,
          paddingTop: 8,
          paddingRight: 8,
          paddingBottom: 8,
          paddingLeft: 8,
          ':hover': {
            backgroundColor: 'rgba(0,0,0,0.05)',
            borderColor: 'transparent',
            boxShadow: `0 0 0 transparent`
          },
          ':focus': {
            backgroundColor: 'rgba(0,0,0,0.05)',
            boxShadow: '0 0 0 transparent'
          },
          ':active': {
            backgroundColor: 'rgba(0,0,0,0.1)',
            borderColor: 'transparent',
            boxShadow: '0 0 0 transparent'
          }
        }
        default: return {
          color: red[500],
          backgroundColor: '#F0F1F1',
          borderColor: '#F0F1F1',
          ':hover': {
            color: red[400],
            backgroundColor: '#F6F6F6',
            borderColor: '#F6F6F6',
            boxShadow: '0 4px 8px 0 rgba(0,0,0,0.2)'
          },
          ':focus': {
            boxShadow: `inset 0 0 0 2px ${red[500]}`
          },
          ':active': {
            backgroundColor: '#E1E1E1',
            borderColor: '#E1E1E1',
            boxShadow: 'inset 0 0 0 0 transparent'
          }
        }
      }
    })()

    const disabledStyles = (() => {
      if(disabled) {
        switch(type) {
          case "primary": return {
            pointerEvents: 'none',
            backgroundColor: red[500],
            borderColor: red[500],
            opacity: '.5'
          }
          case "secondary": return {
            pointerEvents: 'none',
            backgroundColor: gray[500],
            borderColor: gray[500],
            opacity: '.5'
          }
          case "alternate": return {
            pointerEvents: 'none',
            backgroundColor: sky[500],
            borderColor: sky[500],
            opacity: '.5'
          }
          default: return {
            pointerEvents: 'none',
            backgroundColor: red[500],
            borderColor: red[500],
            opacity: '.5'
          }
        }
      } else { return null }
    })()
    return (
      <button type={submit && 'submit'} style={{...defaultStyles, ...(type !== 'icon') && sizeStyles, ...typeStyles, ...disabledStyles, ...style}} onClick={onClick}>
        {(icon) && (
          <Icon
            name={icon}
            style={{
              margin: (children && type !== 'icon') && (iconPosition === 'right' ? '-2px -4px 0 0' : '-2px 0 0 -4px'),
              order: iconPosition === 'right' && 2
          }}/>
        )}
        {children && (<span style={{
          ...truncate,
          marginLeft: icon && iconPosition !== 'right' && 8,
          marginRight: icon && iconPosition === 'right' && 8
        }}>{children}</span>)}
      </button>
    )
  }
}

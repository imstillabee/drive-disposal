import React, { Component } from 'react'
import Portal from './Portal'
import Card from './Card'

export default class Dialog extends Component {
  render({ props } = this) {
    const style = {
      wrap: {
        position: 'fixed',
        top: 0,
        right: 0,
        bottom: 0,
        left: 0,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        zIndex: 99999,
        backgroundColor: 'rgba(0,0,0,0.2)'
      },
      modal: {
        padding: 32,
        maxWidth: 480
      },
      header: {

      },
      title: {
        marginTop: 0,
        lineHeight: 1
      },
      content: {

      },
      footer: {
        marginTop: 24
      }
    }

    return (
      <Portal open={ props.open }>
        <div style={ style.wrap }>
          <Card level={ 8 } style={ style.modal }>
            <div style={ style.header }><h3 style={ style.title }>{ props.title }</h3></div>
            <div style={ style.content }>{ props.children }</div>
            <div style={ style.footer }>
              { props.actions }
            </div>
          </Card>
        </div>
      </Portal>
    )
  }
}
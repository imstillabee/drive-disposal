import React, { Component } from 'react'
import Radium from 'radium'
import { gray } from '../styles/color'
import { shadow } from '../styles/shadow'
import Button from './Button'
import { Grid, Row, Col } from './Grid'
import logo from '../assets/images/logo.svg'
import { ContextMenu, ContextMenuContent, ContextMenuItem } from './ContextMenu'
import { Link } from 'react-router-dom'
import SearchBar from './SearchBar'
import Prefixer from 'inline-style-prefixer'

const prefixer = new Prefixer()

@Radium
export default class AppBar extends Component {
  
  render ({ props: { children, style, title, hasTabs, hideSearchBar, hideTitleBar, collapsed, transparent, scrollTop = 0, searchBar, appActions, pageActions, pages } } = this) {
    const minHeight = 64
    const maxHeight = 200
    const scrollMax = maxHeight - minHeight
    const scrollPercentage = scrollTop <= scrollMax ? scrollTop / scrollMax : 1
    // const appBarHeight = maxHeight - scrollMax * scrollPercentage

    // original - ((original - target)*scrollPercentage)

    const titleFontSize = collapsed ? 16 : 48 - (48 - 16) * scrollPercentage
    const titleMarginBottom = collapsed ? 20 : 40 - (40 - 20) * scrollPercentage

    return (
      <div style={prefixer.prefix({
        position: 'sticky',
        top: -136,
        zIndex: 5
      })}>
        {collapsed && !transparent && <div style={{height: minHeight + (hasTabs ? 40 : 0)}} />}
        <header
          className='appbar'
          style={{
            backgroundColor: !transparent && gray[50],
            boxSizing: 'border-box',
            height: hideTitleBar ? 64 : maxHeight + (hasTabs ? 40 : 0),
            boxShadow: !transparent && shadow[1],
            position: (collapsed || transparent) && 'fixed',
            top: collapsed ? -136 : 0,
            left: 0,
            right: 0,
            ...style
          }}>
          <Grid fluid style={{
            paddingTop: 12,
            paddingBottom: 12,
            position: 'fixed',
            top: 0,
            right: 0,
            left: 0
          }}>
            <Row>
              <Col width={ 3 }>
                <div style={{
                  display: 'inline-block',
                  verticalAlign: 'middle'
                }}>
                  { pages && pages.length && (
                    <ContextMenu style={{
                      position: 'absolute',
                      top: 12,
                      left: 12
                    }}>
                      <Button type='icon' icon='menu' onClick={ () => {} } />
                      <ContextMenuContent>
                        {pages.map((page, i) => (
                          <Link key={ i } to={{ pathname: page.path }} style={{ textDecoration: 'none' }}>
                            <ContextMenuItem
                              label={ page.title }
                              icon={ page.icon }
                              onClick={ () => {} } />
                          </Link>)
                        )}
                      </ContextMenuContent>
                    </ContextMenu>
                  ) }
                  {false
                    && <Button type='icon' icon='chevron-left' />
                  }
                </div>
              </Col>
              <Col width={ 6 }>
                {!hideSearchBar && (<SearchBar { ...(searchBar && searchBar) } />)}
              </Col>
              <Col width={ 3 }>
                <div style={{ marginRight: -52, textAlign: 'right' }}>
                  { appActions && appActions }
                </div>
              </Col>
            </Row>
          </Grid>
          {!hideTitleBar && (
            <Grid fluid style={{
              paddingTop: 64,
              paddingBottom: hasTabs && 40,
              height: '100%',
              boxSizing: 'border-box' }}>
              <Row>
                <Col
                  width={ pageActions ? 6 : 12 }
                  style={{
                    display: 'flex',
                    alignItems: 'flex-end'
                  }}
                >
                  <h1 style={{
                    color: 'black',
                    fontSize: titleFontSize,
                    marginTop: 0,
                    marginBottom: titleMarginBottom
                  }}>
                  
                  
                  
                  {title}</h1>
                </Col>
                { pageActions && <Col width={ 6 }>{pageActions}</Col> }
              </Row>
            </Grid>
          )}
          <div style={{
            height: 40,
            position: 'absolute',
            top: 12,
            left: 64
          }}>
            <Link to='/' style={{ textDecoration: 'none' }}>
              <img src={ logo } className='App-logo' alt='logo' style={{ height: 32, width: 104 }} />
              <span style={{
                fontWeight: 'bold',
                fontSize: 15,
                color: 'black',
                verticalAlign: 'text-bottom'
              }}>operations</span>
            </Link>
          </div>
        </header>
      </div>
    )
  }
}

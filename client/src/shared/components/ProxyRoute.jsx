import React, { Component } from 'react'
import { Route, Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import * as actions from '../utils/actions'
import * as authActions from '../../app/Auth/actions'

const mapStateToProps = (state, ownProps) => {
  return {
    currentRoute: state.currentRoute,
    auth: state.auth
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setCurrentRoute: currentRoute => dispatch(actions.setCurrentRoute(currentRoute)),
    validateUser: () => dispatch(authActions.validateUser())
  }
}

@connect(mapStateToProps, mapDispatchToProps)
export default class ProxyRoute extends Component {
  componentWillMount() {
    this.props.setCurrentRoute(this.props)
    if(this.props.auth.isAuthenticated && !Object.keys(this.props.auth.user).length) {
      this.props.validateUser()
    }
  }

  render ({props, props: {component: Component, config, ...rest}} = this) {
    return this.props.auth.isAuthenticated || props.path === '/login' ? (
      <Route {...rest} render={props => <Component {...props} />} />
    ) : ( <Redirect to="/login" /> )
  }
}

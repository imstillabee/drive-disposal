import React, { Component } from 'react'
import Radium from 'radium'
import { media } from '../styles/responsive'

@Radium
export class Block extends Component {
  render({ props: { children, style } } = this) {
    return (
      <div style={{
        display: 'flex',
        flexDirection: 'column',
        paddingTop: '64px',
        paddingRight: '80px',
        paddingBottom: '64px',
        paddingLeft: '80px',
        ...style,
        [`@media (max-width: ${media.xs}px)`]: {
          paddingTop: '8px',
          paddingRight: '16px',
          paddingBottom: '8px',
          paddingLeft: '16px'
        },
        [`@media (max-width: ${media.sm}px)`]: {
          paddingTop: '16px',
          paddingRight: '32px',
          paddingBottom: '16px',
          paddingLeft: '32px'
        }
      }}>{children}</div>
    )
  }
}

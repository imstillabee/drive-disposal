import React, { Component } from 'react'
import Radium from 'radium'
import Icon from './Icon'
import { gray } from '../styles/color'

@Radium
export default class FilterBar extends Component {
  render({props} = this) {
    const style = {
      bar: {
        width: '100%',
        boxSizing: 'border-box',
        backgroundColor: 'white',
        borderBottomWidth: 1,
        borderBottomColor: gray[200],
        borderBottomStyle: 'solid',
        paddingLeft: 64,
        paddingRight: 64,
        height: 48,
        position: 'relative',
        display: 'flex'
      },
      icon: {
        position: 'absolute',
        top: 12,
        left: 20,
        pointerEvents: 'none'
      },
      input: {
        backgroundColor: 'transparent',
        border: 0,
        fontSize: 16,
        marginTop: 0,
        marginRight: 0,
        marginBottom: 0,
        marginLeft: -64,
        paddingTop: 16,
        paddingRight: 0,
        paddingBottom: 16,
        paddingLeft: 64,
        lineHeight: '16px',
        outline: 'none',
        boxSizing: 'border-box',
        width: '100%'
      },
      left: { flex: '1' },
      right: {}
    }
    return (
      <div style={{...style.bar, ...props.style}}>
        <div style={{...style.left}}>
          <Icon name="search" style={{...style.icon}} />
          <input type="text" onChange={(e)=>{props.onFilterChange(e.target.value)}} placeholder="Filter items" style={{...style.input}} />
        </div>
        <div style={{...style.right}}>

        </div>
      </div>
    )
  }
}

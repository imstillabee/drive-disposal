import React from 'react';
import PropTypes from 'prop-types';

const LineComparison = ({ total, ssdCount }) => {
  console.log('here', total, ssdCount)
  let hddCount = total - ssdCount
  let percent = hddCount / total * 100
  let bar = { width: `${percent}%` }

  return (
    <div className='disk-comparison' style={{display: 'block', clear: 'both', width: '100%'}}>
      <div className='counter'>HDD <p>{hddCount}</p></div>
      <div className='line-chart' style={{width: '80%'}}>
        <div style={ bar } className='hard-disk'></div>
      </div>
      <div className='counter'>SSD <p>{ssdCount}</p></div>
    </div>
  )
}

LineComparison.propTypes = {
  total: PropTypes.number,
  ssdCount: PropTypes.number
}

export default LineComparison

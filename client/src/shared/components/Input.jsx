import React, { Component } from 'react'
import Radium from 'radium'
import Icon from './Icon'

import { red, gray } from '../styles/color'

@Radium
export default class Input extends Component {
  constructor(props) {
    super(props)
    this.state = { value: props.value || '' }
    this.onChange = this.onChange.bind(this)
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.value !== this.state.value) {
      this.setState({ value: nextProps.value });
    } else if (nextProps.autoFocus !== this.props.autoFocus && nextProps.autoFocus) {
      this.inputEl.focus()
    }
  }

  onChange(event) {
    let value
    event.target.type === 'number' ? value = parseInt(event.target.value, 10) : value = event.target.value
    Array.isArray(this.props.category.slice())
      ? this.props.onChange(this.props.category.slice().concat([event.target.name]), value)
      : this.props.onChange([this.props.category, event.target.name], value)
  }

  render({ props: { children, style, className, defaultValue, id, pattern, title, name, max, onChange, onClick, onKeyUp, size, value, label, type, disabled, autoFocus, hasError, errorMessage, onboarding, inputRef, icon, description } } = this) {
    const defaultValueStyle = (defaultValue && defaultValue !== value)
      ? { fontFamily: 'Neue Haas Grotesk Display Std', color: red[500] }
      : null
    const inputDefaultStyles = {
      outline: 'none',
      borderTopWidth: 0,
      borderRightWidth: 0,
      borderLeftWidth: 0,
      borderTopColor: 'transparent',
      borderRightColor: 'transparent',
      borderLeftColor: 'transparent',
      borderBottomWidth: 2,
      borderBottomStyle: 'solid',
      borderBottomColor: gray[300],
      backgroundColor: 'transparent',
      width: '100%',
      height: 40,
      paddingTop: 4,
      paddingRight: 0,
      paddingBottom: 4,
      paddingLeft: icon ? 40 : 0,
      fontFamily: 'Neue Haas Grotesk Text Std',
      fontSize: 16,
      boxSizing: 'border-box',
      transition: '.2s',
      ...defaultValueStyle
    }
    const inputHoverStyles = {
      borderBottomColor: hasError ? red[500] : gray[500]
    }
    const inputFocusStyles = {
      borderBottomColor: red[500]
    }
    const inputDisabledStyles = {
      borderBottomStyle: 'dotted',
      borderBottomColor: gray[300],
    }
    const inputErrorStyles = {
      color: red[500],
      borderBottomColor: red[500]
    }

    const labelDefaultStyles = {
      opacity: value ? 1 : 0,
      fontSize: 12,
      color: gray[500],
      display: 'block',
      transition: '.2s',
      paddingLeft: icon && 40
    }
    const labelHoverStyles = {
      color: hasError ? red[500] : gray[600]
    }
    const labelFocusStyles = {
      color: gray[900]
    }
    const labelDisabledStyles = {
      color: gray[500],
    }
    const labelErrorStyles = {
      color: red[500]
    }
    return (
      <div key={id} style={{ display: 'inline-block', position: 'relative', ...style }}>
        {hasError && <span style={{ color: red[500], fontSize: 14 }}>{errorMessage}</span>}
        <div style={{ fontSize: 14 }}>{ description && description }</div>
        <div style={{ position: 'relative' }}>
          {icon && (
            <Icon name={icon} style={{ position: 'absolute', top: 8, left: 0, pointerEvents: 'none' }} />
          )}
          <input
            name={name}
            type={type}
            placeholder={label}
            value={value}
            id={id}
            pattern={pattern}
            title={title}
            onKeyUp={onKeyUp}
            autoFocus={(autoFocus) ? 'true' : null}
            disabled={(disabled) ? 'true' : null}
            onChange={(onboarding) ? this.onChange : onChange}
            key={id + '-input'}
            maxLength={max}
            ref={(el) => { this.inputEl = el }}
            style={{
              ...inputDefaultStyles,
              ...(Radium.getState(this.state, id + '-label', ':hover')) && inputHoverStyles,
              ...(disabled) && inputDisabledStyles,
              ...(hasError) && inputErrorStyles,
              ':hover': inputHoverStyles,
              ':focus': inputFocusStyles
            }} />
        </div>
        <label key={id + '-label'} style={{
          ...labelDefaultStyles,
          ...(Radium.getState(this.state, id + '-input', ':hover')) && labelHoverStyles,
          ...(Radium.getState(this.state, id + '-input', ':focus')) && labelFocusStyles,
          ...(disabled) && labelDisabledStyles,
          ...(hasError) && labelErrorStyles,
          ':hover': inputHoverStyles
        }} htmlFor={id}>{label ? label : '\u00A0'}</label>
      </div>
    )
  }
}

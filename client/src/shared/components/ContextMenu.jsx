import React from 'react'
import ReactDOM from 'react-dom'
import Radium from 'radium'
import Transition from 'react-transition-group/Transition';
import Icon from './Icon'
import {gray} from '../styles/color'
import {shadow} from '../styles/shadow'
import Portal from './Portal'

export class ContextMenu extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      showMenu: false,
      menuDimensions: {}
    }
  }

  componentDidMount() {
    window.addEventListener('click', this.handleClickOutside, true)
  }

  componentWillUnmount () {
    window.removeEventListener('click', this.handleClickOutside, true)
  }

  handleClickOutside = (event) => {
    const domNode = ReactDOM.findDOMNode(this);
    const menuDomNode = ReactDOM.findDOMNode(this.refs['ContextMenuContent'])
    if (this.state.showMenu && (!domNode || !domNode.contains(event.target)) && (!menuDomNode || !menuDomNode.contains(event.target))) {
       this.handleTriggerClick()
    }
  }

  handleTriggerClick = (e) => {
    this.setState({
      showMenu: !this.state.showMenu,
      triggerDimensions: ReactDOM.findDOMNode(this.refs['contextMenuTrigger']).getBoundingClientRect()
    },() => {
      //TODO: Fix inaccurate menu height retrieved via getBoundingClientRect - at this point, it has a max-height of 0
      if(this.state.showMenu) {
        this.setState({
          menuDimensions: ReactDOM.findDOMNode(this.refs['ContextMenuContent']).getBoundingClientRect()
        })
      }
    })
  }

  render() {
    const childrenWithProps = React.Children.map(this.props.children,
      (child) => {
        if(typeof child.type === 'string') {
          return React.cloneElement(child, {
            onClick: () => {
              if(typeof child.props.onClick !== 'undefined') child.props.onClick()
              this.handleTriggerClick()
            }
          })
        } else if (typeof child.type === 'function') {
          if(child.type.name !== 'undefined') {
            if(child.type.name === 'ContextMenuContent') {
              const el = React.cloneElement(child, {
                ref: 'ContextMenuContent',
                toggleMenu: this.handleTriggerClick,
                showMenu: this.state.showMenu,
                theme: this.props.theme,
                triggerDimensions: this.state.triggerDimensions,
                menuDimensions: this.state.menuDimensions,
                menuOrigin: this.props.menuOrigin,
                triggerOrigin: this.props.triggerOrigin
              })
              return (<div>{el}</div>)
            } else {
              return React.cloneElement(child, {
                onClick: (e) => {
                  if(typeof child.props.onClick !== 'undefined') child.props.onClick()
                  this.handleTriggerClick(e)
                },
                ref: 'contextMenuTrigger'
              })
            }
          } else { return React.cloneElement(child) }
        } else { return React.cloneElement(child) }
      }
    )

    return (
      <div
        ref={'contextMenu'}
        className="context-menu"
        style={{
          position: 'relative',
          ...this.props.style
        }}>
        {childrenWithProps}
      </div>
    )
  }
}

export class ContextMenuContent extends React.Component {

  constructor() {
    super()
    this.state = { menuDimensions: null }
  }

  componentDidMount() {

  }
  componentWillReceiveProps() {

  }

  render({props: {showMenu, triggerDimensions, triggerOrigin, menuOrigin, menuDimensions }} = this) {
    const childrenWithProps = React.Children.map(this.props.children,
      (child) => {
        return React.cloneElement(child, {
          onClick: () => {
            if(typeof child.props.onClick !== 'undefined') {
              child.props.onClick()
            }
            this.props.toggleMenu()
          }, theme: this.props.theme
        })
      }
    )
    const duration = 400
    const style = {
      content: {
        default: {
          position: 'fixed',
          top: triggerDimensions ? triggerDimensions.bottom : 0,
          left: triggerDimensions ? triggerDimensions.left : 0,
          width: 240,
          backgroundColor: this.props.theme === 'dark' ? '#222222' : gray[50],
          borderRadius: 2,
          boxShadow: shadow[2],
          paddingTop: 8,
          paddingBottom: 8,
          overflow: 'hidden',
          boxSizing: 'border-box',
          transition: `opacity ${duration/1.5}ms, max-height ${duration}ms cubic-bezier(.7,0,.3,1)`,
          zIndex: 50,
          ...(triggerDimensions && menuDimensions) && {
            ...(menuOrigin && menuOrigin.vertical === 'top') && {
              ...(triggerOrigin && triggerOrigin.vertical === 'top') && {
                top: triggerDimensions.top
              },
              ...(triggerOrigin && triggerOrigin.vertical === 'center') && {
                top: triggerDimensions.top + triggerDimensions.height/2
              },
              ...(triggerOrigin && triggerOrigin.vertical === 'bottom') && {
                top: triggerDimensions.bottom
              }
            },
            ...(menuOrigin && menuOrigin.vertical === 'center') && {
              ...(triggerOrigin && triggerOrigin.vertical === 'top') && {
                top: triggerDimensions.top - menuDimensions.height/2
              },
              ...(triggerOrigin && triggerOrigin.vertical === 'center') && {
                top: (triggerDimensions.top + triggerDimensions.height/2) - menuDimensions.height/2
              },
              ...(triggerOrigin && triggerOrigin.vertical === 'bottom') && {
                top: triggerDimensions.bottom - menuDimensions.height/2
              }
            },
            ...(menuOrigin && menuOrigin.vertical === 'bottom') && {
              ...(triggerOrigin && triggerOrigin.vertical === 'top') && {
                top: triggerDimensions.top - menuDimensions.height
              },
              ...(triggerOrigin && triggerOrigin.vertical === 'center') && {
                top: (triggerDimensions.top + triggerDimensions.height/2) - menuDimensions.height
              },
              ...(triggerOrigin && triggerOrigin.vertical === 'bottom') && {
                top: triggerDimensions.bottom - menuDimensions.height
              }
            },

            ...(menuOrigin && menuOrigin.horizontal === 'left') && {
              ...(triggerOrigin && triggerOrigin.horizontal === 'left') && {
                left: triggerDimensions.left
              },
              ...(triggerOrigin && triggerOrigin.horizontal === 'center') && {
                left: triggerDimensions.left + triggerDimensions.width/2
              },
              ...(triggerOrigin && triggerOrigin.horizontal === 'right') && {
                left: triggerDimensions.right
              }
            },
            ...(menuOrigin && menuOrigin.horizontal === 'center') && {
              ...(triggerOrigin && triggerOrigin.horizontal === 'left') && {
                left: triggerDimensions.left - menuDimensions.width/2
              },
              ...(triggerOrigin && triggerOrigin.horizontal === 'center') && {
                left: (triggerDimensions.left + triggerDimensions.width/2) - menuDimensions.width/2
              },
              ...(triggerOrigin && triggerOrigin.horizontal === 'right') && {
                left: triggerDimensions.right - menuDimensions.width/2
              }
            },
            ...(menuOrigin && menuOrigin.horizontal === 'right') && {
              ...(triggerOrigin && triggerOrigin.horizontal === 'left') && {
                left: triggerDimensions.left - menuDimensions.width
              },
              ...(triggerOrigin && triggerOrigin.horizontal === 'center') && {
                left: (triggerDimensions.left + triggerDimensions.width/2) - menuDimensions.width
              },
              ...(triggerOrigin && triggerOrigin.horizontal === 'right') && {
                left: triggerDimensions.right - menuDimensions.width
              }
            }
          }
        },
        entering:  {
          opacity: 0,
          maxHeight: 0
        },
        entered: {
          opacity: 1,
          maxHeight: 500
        },
        exiting:  {
          opacity: 0,
          maxHeight: 0,
          transition: `opacity ${duration}ms, max-height ${duration/1.5}ms`,
        },
        exited:  {
          opacity: 0,
          maxHeight: 0,
          pointerEvents: 'none'
        }
      },
      inside: {
        default: {
          transition: `opacity ${duration/1.5}ms, transform ${duration}ms cubic-bezier(.7,0,.3,1)`,
          transform: 'translateY(-16px)'
        },
        entering: {
          opacity: 0,
          transform: 'translateY(-16px)'
        },
        entered: {
          opacity: 1,
          transform: 'translateY(0)'
        },
        exiting:  {
          opacity: 0,
          transform: 'translateY(-16px)',
          transition: `opacity ${duration}ms, transform ${duration/1.5}ms cubic-bezier(.7,0,.3,1)`,
        },
        exited:  {
          opacity: 0,
          transform: 'translateY(-16px)'
        }
      }
    }
    return (
      <Transition
        in={showMenu}
        mountOnEnter
        unmountOnExit
        timeout={{
         enter: 0,
         exit: duration,
        }}>
        {(state) => {
          return (
            <Portal open={true}>
              <div ref={(el) => { this.contextMenuEl = el }} style={{
                ...style.content.default,
                ...style.content[state]
              }}>
                <div className="context-menu-inside" style={{
                  ...style.inside.default,
                  ...style.inside[state]
                }}>
                  {childrenWithProps}
                </div>
              </div>
            </Portal>
        )}}
      </Transition>
    )
  }
}

@Radium
export class ContextMenuItem extends React.Component {
  render({props} = this) {
    return(
      <div onClick={this.props.onClick} style={{
        fontSize: 15,
        padding: '8px 16px',
        lineHeight: '20px',
        transition: '200ms',
        cursor: 'pointer',
        color: this.props.theme === 'dark' ? gray[500] : gray[800],
        ':hover': {
          color: this.props.theme === 'dark' ? '#ffffff' : gray[800],
          backgroundColor: this.props.theme === 'dark' ? '#171717' : gray[200]
        },
        ':active': {
          backgroundColor: this.props.theme === 'dark' ? '#111111' : gray[300]
        }
      }}>
        <Icon
          name={this.props.icon}
          style={{
            verticalAlign: 'middle',
            marginRight: 16
          }} />
        {this.props.label}
      </div>
    )
  }
}

@Radium
export class ContextMenuDivider extends React.Component {
  render({props} = this) {
    return(
      <div style={{
        borderTop: `1px solid ${this.props.theme === 'dark' ? gray[600] : gray[300]}`,
        margin: '8px 0'
      }} />
    )
  }
}

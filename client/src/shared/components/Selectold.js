import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import Radium from 'radium'
import { red, gray } from '../styles/color'
import { shadow } from '../styles/shadow'
import { truncate } from '../styles/typography'

@Radium
export default class Select extends Component {
  constructor (props) {
    super(props)

    let value = []

    if (props.value.length) {
      props.options.forEach(option => {
        if (props.multiple) {
          props.value.forEach(val => {
            if (val === option.value)
              value.push(option)
          })
        } else if (props.value === option.value)
          value.push(option)
      })
    }

    this.state = {
      hoverIndex: 0,
      selectedIndex: 0,
      tagsHeight: 0,
      open: false,
      hasFocus: props.autoFocus || false,
      searchQuery: '',
      filteredOptions: props.options || [],
      value: value
    }
    this.toggleSelect = this.toggleSelect.bind(this)
    this.handleClick = this.handleClick.bind(this)
    this.handleSearchChange = this.handleSearchChange.bind(this)
    this.filterOptions = this.filterOptions.bind(this)
    this.deselectOption = this.deselectOption.bind(this)
    this.handleClickOutside = this.handleClickOutside.bind(this)
  }

  componentWillReceiveProps (nextProps) {
    if (nextProps.value !== this.props.value) {
      if (nextProps.value && !this.props.multiple) {
        this.props.options.forEach((option, i) => {
          if (option.value === nextProps.value)
            this.setState({ selectedIndex: i + 1 }, () => false)
        })
      } else  this.setState({ selectedIndex: 0 }, () => false)
    }
  }

  componentDidMount () {
    if (this.props.value && !this.props.multiple) {
      this.props.options.forEach((option, i) => {
        if (option.value === this.props.value)
          this.setState({ selectedIndex: i + 1 })
      })
    }
    if (this.props.multiple) this.setTagsHeight()
    window.addEventListener('click', this.handleClickOutside, true)
  }

  componentWillUnmount () {
    window.removeEventListener('click', this.handleClickOutside, true)
  }

  handleClickOutside (event) {
    const domNode = ReactDOM.findDOMNode(this)

    if (!domNode || !domNode.contains(event.target)) {
      this.setState({
        open: false,
        searchQuery: ''
      }, () => {
        this.setState({ filteredOptions: this.filterOptions() }, () => {
          this.refs.container.offsetParent.scrollTop = 0
        })
      })
    }
  }

  onboardingChange = value => {
    Array.isArray(this.props.category.slice())
      ? this.props.onChange(this.props.category.slice(), value)
      : this.props.onChange([this.props.category], value)
  }

  toggleSelect () {
    this.setState({ open: !this.state.open, searchQuery: '' }, () => {
      this.setState({ filteredOptions: this.filterOptions() })
      if (this.props.searchable && this.state.open)
        window.setTimeout(() => { this[this.props.id + 'SelectSearch'].focus() }, 500)
    })
  }

  handleClick (selection, index, e) {
    if (this.props.multiple) {
      let newOptions = this.state.value.slice()

      for (let i in newOptions)
        if (selection.value === newOptions[i]) return false

      newOptions = newOptions.concat([selection])
      this.setState({ value: newOptions, selectedIndex: index, searchQuery: '' }, () => {
        this.setState({ filteredOptions: this.filterOptions() })
        this.props.onChange(this.state.value.map(val => val.value))
      })
      window.setTimeout(() => { this.setTagsHeight() })
    } else {
      this.setState({ value: [selection], selectedIndex: index, searchQuery: '' }, () => {
        this.setState({ filteredOptions: this.filterOptions() }, () => {
          this.refs.container.offsetParent.scrollTop = 0
        })
        if (this.props.onboarding)
          this.onboardingChange(this.state.value[0].value)
        else
          this.props.onChange(this.state.value[0].value)
      })
    }
    if (this.props.searchable && this.state.open && this.props.multiple)
      window.setTimeout(() => { this[this.props.id + 'SelectSearch'].focus() }, 250)
  }

  filterOptions () {
    return this.props.options.filter(option => this.state.value.filter(selectedOption => selectedOption.value === option.value && this.props.multiple).length === 0 && option.label.toLowerCase().indexOf(this.state.searchQuery.toLowerCase()) > -1)
  }

  handleSearchChange (e) {
    this.setState({ searchQuery: e.target.value }, () => {
      this.setState({ filteredOptions: this.filterOptions() })
    })
  }

  setTagsHeight () {
    const tagsHeight = this[this.props.id + 'TagWrap'].clientHeight

    this.setState({ tagsHeight })
  }

  deselectOption (optionToRemove) {
    const selectedOptions = this.state.value.filter(option => option !== optionToRemove)

    this.setState({ value: selectedOptions }, () => {
      this.setState({ filteredOptions: this.filterOptions() }, () => { this.setTagsHeight() })
      this.props.onChange(selectedOptions.map(option => option.value))
    })
  }

  render ({ props: { children, style, className, defaultValue, id, name, max, onChange, onClick, onKeyUp, value, label, disabled, autoFocus, hasError, errorMessage, onboarding, options, searchable, multiple } } = this) {
    const defaultValueStyle = defaultValue && defaultValue !== value
      ? { fontFamily: 'Neue Haas Grotesk Display Std', color: red[500] }
      : null

    const selectHeight = 32

    const transitionTime = '200ms'

    const selectHide = {
      opacity: 0,
      position: 'absolute',
      left: '-99999px'
    }

    const selectWrap = {
      position: 'relative',
      zIndex: this.state.open && 99999,
      height: multiple && this.state.value.length ? this.state.tagsHeight : selectHeight,
      cursor: 'pointer',
      marginBottom: 2,
      borderTopWidth: 0,
      borderRightWidth: 0,
      borderLeftWidth: 0,
      borderTopColor: 'transparent',
      borderRightColor: 'transparent',
      borderLeftColor: 'transparent',
      borderBottomWidth: 2,
      borderBottomStyle: 'solid',
      borderBottomColor: gray[300],
      transition: transitionTime,
      ':active': {
        borderBottomColor: Radium.getState(this.state, id + '-select-wrap', ':hover') && !this.state.open && red[500]
      },
      ...defaultValueStyle
    }

    const selectHoverStyles = {
      borderBottomColor: hasError ? red[500] : gray[500]
    }

    const selectFocusStyles = {
      borderBottomColor: red[500]
    }

    const selectDisabledStyles = {
      borderBottomStyle: 'dotted',
      borderBottomColor: gray[300]
    }

    const selectErrorStyles = {
      color: red[500],
      borderBottomColor: red[500]
    }

    const dropdownIndicator = {
      height: selectHeight,
      backgroundColor: this.state.open ? gray[200] : 'transparent',
      position: 'absolute',
      top: this.state.hoverIndex * selectHeight + this.state.tagsHeight,
      left: 0,
      right: 0,
      transition: transitionTime
    }

    const dropdownIndicatorActive = {
      backgroundColor: gray[300]
    }

    const dropdownWrap = {
      backgroundColor: this.state.open ? gray[50] : 'transparent',
      position: 'absolute',
      top: 0,
      left: 0,
      width: '100%',
      height:
        this.state.open
          ? selectHeight * (
            this.state.filteredOptions.length
              ? this.state.filteredOptions.length + 1
              : this.state.value.length === options.length ? 1 : 2
          ) + this.state.tagsHeight
          : multiple && this.state.value.length
            ? this.state.tagsHeight
            : selectHeight,
      borderRadius: 2,
      boxShadow: this.state.open ? shadow[4] : shadow[0],
      transition: transitionTime,
      overflow: this.state.open ? 'scroll' : 'hidden',
      maxHeight: 500
    }

    const dropdownOptions = {
      position: 'absolute',
      top: this.state.open || multiple ? 0 : -(this.state.selectedIndex * selectHeight),
      width: '100%',
      transition: transitionTime
    }

    const arrow = {
      width: 8,
      height: 8,
      borderTopWidth: 2,
      borderTopStyle: 'solid',
      borderTopColor: Radium.getState(this.state, id + '-select-wrap', ':hover') && !this.state.open ? red[500] : gray[500],
      borderRightWidth: 2,
      borderRightStyle: 'solid',
      borderRightColor: Radium.getState(this.state, id + '-select-wrap', ':hover') && !this.state.open ? red[500] : gray[500],
      position: 'absolute',
      bottom: this.state.open ? 8 : 14,
      right: this.state.open ? 12 : 8,
      transform: this.state.open ? 'rotate(-45deg)' : 'rotate(135deg)',
      transition: transitionTime
    }

    const arrowWrap = {
      display: 'block',
      position: 'absolute',
      top: 0,
      right: 0,
      width: 40,
      height:
        this.state.open
          ? this.state.value.length === options.length
            ? this.state.tagsHeight
            : selectHeight + this.state.tagsHeight
          : multiple && this.state.value.length
            ? this.state.tagsHeight
            : selectHeight,
      transition: transitionTime
    }

    const dropdownPlaceholder = {
      color: Radium.getState(this.state, id + '-select-wrap', ':hover') && !this.state.open && !multiple ? red[500] : gray[500],
      boxShadow: this.state.open && 'inset 0 -1px 0 0 ' + gray[300]
    }

    const dropdownOption = {
      color: gray[900],
      fontSize: this.state.open ? 14 : 16,
      display: 'block',
      lineHeight: selectHeight + 'px',
      height: selectHeight,
      textIndent: this.state.open ? '12px' : 0,
      position: 'relative',
      transition: transitionTime,
      pointerEvents: !this.state.open && 'none',
      paddingRight: 40,
      ...truncate
    }

    const dropdownSelectedOption = {
      color: Radium.getState((this.state, id + '-select-wrap', ':hover') || this.state.open) && !multiple && red[500]
    }

    const labelDefaultStyles = {
      opacity: String(value).length ? 1 : 0,
      fontSize: 12,
      color: gray[500],
      display: 'block',
      transition: '.2s',
      position: 'absolute'
    }
    const labelHoverStyles = { color: hasError ? red[500] : gray[600] }
    const labelFocusStyles = { color: gray[900] }
    const labelDisabledStyles = { color: gray[500] }
    const labelErrorStyles = { color: red[500] }

    return (
      <div style={{ ...style }}>
        {hasError && <span style={{ color: red[500], fontSize: 14 }}>{errorMessage}</span>}
        <div
          key={ id + '-select-wrap' }
          style={{
            ...selectWrap,
            ...this.state.hasFocus && selectFocusStyles,
            ...disabled && selectDisabledStyles,
            ...hasError && selectErrorStyles,
            ':hover': selectHoverStyles,
            ':active': {}
          }}>
          <select
            multiple={ multiple }
            name={ name }
            placeholder={ label }
            value={ !value && multiple ? [] : value }
            id={ id }
            onKeyUp={ onKeyUp }
            autoFocus={ autoFocus ? 'true' : null }
            disabled={ disabled ? 'true' : null }
            onChange={ onboarding ? this.onboardingChange : onChange }
            key={ id + '-select' }
            onFocus={ () => { this.setState({ hasFocus: true }) } }
            onBlur={ () => { this.setState({ hasFocus: false, open: false }) } }
            style={{
              ...selectHide,
              ':focus': {}
            }}>
            <option key={ id + '-select-option-placeholder' } value=''>{label}</option>
            {options && options.map(option =>
              <option
                key={ id + '-select-option-' + String(option.value).replace(/\s+/g, '-')
                .toLowerCase() }
                value={ option.value }>{option.label}</option>
            )}
          </select>
          <span
            key={ id + '-dropdown-wrap' }
            style={ disabled ? { ...dropdownWrap, cursor: 'default' } : dropdownWrap }
            onClick={ disabled ? null : () => { if ((multiple && !this.state.open) || !multiple) this.toggleSelect() } }>

            <span style={ dropdownOptions } ref='container'>
              <span style={{
                ...dropdownIndicator,
                ...Radium.getState(this.state, id + '-select-wrap', ':active') && this.state.open && dropdownIndicatorActive
              }}></span>

              {multiple
                && <span
                  ref={ tagWrap => this[id + 'TagWrap'] = tagWrap } style={{
                    display: 'block',
                    backgroundColor: this.state.open && searchable ? 'white' : 'transparent',
                    paddingRight: 40,
                    marginLeft: !this.state.open ? '-4px' : '0',
                    transition: transitionTime
                  }}>
                  {this.state.value.map(option =>
                    <span
                      key={ id + '-option-' + option.value } style={{
                        backgroundColor: searchable || !this.state.open ? gray[100] : gray[200],
                        fontSize: '13px',
                        lineHeight: '24px',
                        padding: '0 12px',
                        borderRadius: 24,
                        display: 'inline-block',
                        margin: 4,
                        transition: transitionTime
                      }}>
                      <span style={{
                        display: 'inline-block'
                      }}>{option.label}</span>
                      <span
                        onClick={ e => { e.stopPropagation(); this.deselectOption(option); return false } }
                        style={{
                          display: 'inline-block',
                          fontWeight: '100',
                          fontFamily: 'sans-serif',
                          fontSize: '16px',
                          lineHeight: '24px',
                          marginRight: '-12px',
                          padding: '0 8px 0 4px'
                        }}>×</span>
                    </span>
                  )}
                </span>
              }

              {searchable
                ? this.state.value.length !== options.length
                  && <input
                    ref={ selectSearch => { this[id + 'SelectSearch'] = selectSearch } }
                    value={ this.state.searchQuery }
                    onChange={ this.handleSearchChange }
                    tabIndex='-1'
                    type='text' placeholder={ label } style={{
                      ...dropdownOption,
                      ...dropdownPlaceholder,
                      width: '100%',
                      backgroundColor: this.state.open ? 'white' : 'transparent',
                      borderBottom: this.state.open ? '2px solid ' + red[500] : '2px solid transparent',
                      boxShadow: '0 0 0 0 transparent',
                      color: gray[900]
                    }} />
                :                <span
                  key={ id + '-dropdown-option-placeholder' }
                  onClick={ e => { this.toggleSelect() } }
                  style={{
                    ...dropdownOption,
                    ...dropdownPlaceholder,
                    ':active': {}
                  }}>{label}</span>
              }

              {this.state.filteredOptions.map((option, index) =>
                <span
                  key={ id + '-dropdown-option-' + String(option.value).replace(/\s+/g, '-')
                  .toLowerCase() }
                  onMouseOver={ () => { this.setState({ hoverIndex: index + 1 }) } }
                  onClick={ e => { this.handleClick(option, index + 1, e) } }
                  style={{
                    ...dropdownOption,
                    ...this.state.selectedIndex === index + 1 && dropdownSelectedOption,
                    ':active': {}
                  }}>{option.label}</span>
              )}
              {!this.state.filteredOptions.length && options.length && this.state.value.length !== options.length
                && <span
                  key={ id + '-dropdown-option-placeholder' }
                  onMouseOver={ () => { this.setState({ hoverIndex: 0 }) } }
                  onClick={ e => { this.handleClick({}, 0, e) } }
                  style={{
                    ...dropdownOption,
                    ...dropdownPlaceholder,
                    pointerEvents: 'none',
                    boxShadow: '0 0 0 0 transparent'
                  }}>No results found</span>
              }
              {multiple && this.state.filteredOptions.length === 0 && this.state.value.length === options.length
                && <span
                  key={ id + '-dropdown-option-placeholder' }
                  onMouseOver={ () => { this.setState({ hoverIndex: 0 }) } }
                  onClick={ e => { this.handleClick({}, 0, e) } }
                  style={{
                    ...dropdownOption,
                    ...dropdownPlaceholder,
                    pointerEvents: 'none',
                    boxShadow: '0 0 0 0 transparent'
                  }}>No more options</span>
              }
            </span>
            <span style={ arrowWrap } onClick={ this.toggleSelect }>
              <span style={ arrow }></span>
            </span>
          </span>
        </div>
        <label
          key={ id + '-label' } style={{
            ...labelDefaultStyles,
            ...Radium.getState(this.state, id + '-select-wrap', ':hover') && labelHoverStyles,
            ...Radium.getState(this.state, id + '-select-wrap', ':focus') && labelFocusStyles,
            ...disabled && labelDisabledStyles,
            ...hasError && labelErrorStyles,
            ':hover': labelHoverStyles
          }} htmlFor={ id }>{label}</label>
      </div>
    )
  }
}

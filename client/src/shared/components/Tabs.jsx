import React, { Component } from 'react'
import { Switch, Route, Redirect, NavLink } from 'react-router-dom'
import { red } from '../styles/color'
import Prefixer from 'inline-style-prefixer'

const prefixer = new Prefixer()

export const Tab = props => {
  const tabStyles = {
    display: 'inline-block',
    color: 'black',
    boxShadow: props.isActive && !props.baseRoute && `inset 0 -3px 0 ${red[500]}`,
    paddingTop: 0,
    paddingBottom: 16,
    marginLeft: 12,
    marginRight: 12,
    fontSize: 14,
    fontWeight: 'bold',
    cursor: 'pointer',
    textDecoration: 'none'
  }
  return (
    <li style={{ display: 'inline-block' }}>
      {props.baseRoute ? (
        <NavLink
          to={`${props.baseRoute}/${props.label.replace(/\s+/g, '-').toLowerCase()}`}
          replace
          style={tabStyles}
          activeStyle={{ boxShadow: `inset 0 -3px 0 ${red[500]}` }}
          onClick={ e => {
            props.onClick(props.tabIndex)
          } }>{ props.label }</NavLink>
      ) : (
        <a style={tabStyles} onClick={ e => {
          e.preventDefault()
          props.onClick(props.tabIndex)
        } }>{ props.label }</a>
      )}
    </li>
  )
}

export class Tabs extends Component {
  constructor (props, context) {
    super(props, context)
    this.state = {
      activeTabIndex: this.props.defaultActiveTabIndex || 0
    }
    this.handleTabClick = this.handleTabClick.bind(this)
  }

  // Toggle currently active tab
  handleTabClick (tabIndex) {
    this.setState({
      activeTabIndex: tabIndex
    })
  }

  // Encapsulate <Tabs/> component API as props for <Tab/> children
  renderChildrenWithTabsApiAsProps () {
    return React.Children.map(this.props.children, (child, index) => {
      return React.cloneElement(child, {
        onClick: this.handleTabClick,
        tabIndex: index,
        isActive: index === this.state.activeTabIndex,
        baseRoute: this.props.baseRoute,
        tabId: child.props.label.replace(/\s+/g, '-').toLowerCase()
      })
    })
  }

  // Render current active tab content
  renderActiveTabContent () {
    const { children } = this.props
    const { activeTabIndex } = this.state
    if (children[activeTabIndex]) {
      return children[activeTabIndex].props.children
    }
  }

  render ({ props } = this) {
    return (
      <div className='tabs' style={{
        display: 'flex',
        flexDirection: 'column',
        ...props.style
      }}>
        <div style={prefixer.prefix({
          margin: 0,
          padding: 0,
          marginTop: -39,
          position: 'sticky',
          top: 64,
          zIndex: 5
        })}>
          <ul style={{ margin: 0, paddingLeft: 52 }}>
            { this.renderChildrenWithTabsApiAsProps() }
          </ul>
        </div>
        { props.baseRoute ? (
          <div style={{flex: '1', position: 'relative'}}>
            <Switch>
              {props.children.map((child, index) => {
                return (<Route key={index} path={`${props.baseRoute}/${child.props.label.replace(/\s+/g, '-').toLowerCase()}`} render={() => child.props.children}/>)
              })}
              <Redirect from={`${props.baseRoute}`} exact to={`${props.baseRoute}/${props.children[0].props.label.replace(/\s+/g, '-').toLowerCase()}`} />
            </Switch>
          </div>
        ) : (
          <div className='tabs-active-content'>
            { this.renderActiveTabContent() }
          </div>
        ) }
      </div>
    )
  }
}

import React, { Component } from 'react'
import Radium from 'radium'
import {red, gray, hexToRgb} from '../styles/color'

@Radium
export default class Checkbox extends Component {
  constructor(props) {
     super(props)
     this.state = {}
  }

  render({ props: {id, style, className, onClick, label, name, size, checked, disabled, value, onChange} } = this) {

    const sizeVal = (() => {
      switch(size) {
        case "sm": return 16
        case "md": return 24
        case "lg": return 32
        default: return 24
      }
    })()

    const boxHoverStyles = {
      backgroundColor: disabled ? gray[300] : (checked ? red[400] : 'white'),
      borderColor: disabled ? gray[300] : (checked ? red[400] : gray[500]),
      boxShadow: checked ? '0 4px 8px 0 rgba(' + hexToRgb(red[500]).r + ', ' + hexToRgb(red[500]).g + ', ' + hexToRgb(red[500]).b + ', 0.33)' : '0 4px 8px 0 rgba(0,0,0, 0.15)'
    }

    const boxActiveStyles = {
      backgroundColor: disabled ? gray[300] : (checked ? red[600] : gray[200]),
      borderColor: disabled ? gray[300] : (checked ? red[600] : gray[500]),
      boxShadow: '0 0 0 0 transparent'
    }

    const boxFocusStyles = {
      backgroundColor: disabled ? gray[300] : (checked ? red[400] : 'white'),
      borderColor: disabled ? gray[300] : (checked ? 'red[900]' : red[500]),
    }


    return (
      <div style={{
          display: 'inline-block',
          marginRight: label && sizeVal,
          width: sizeVal,
          height: sizeVal
        }}>
        <input
          name={name}
          type="checkbox"
          key={id}
          id={id}
          disabled={disabled}
          checked={checked}
          onChange={onChange}
          value={value}
          style={{
            position: 'absolute',
            left: '-99999px',
            opacity: 0,
            ':focus': {}
          }} />
        <label ref={id+'-label'} onClick={onClick} style={{
          width: !label && sizeVal,
          height: sizeVal,
          ':hover': {},
          ':active': {}
        }} htmlFor={id}>
          <span style={{
            height: sizeVal,
            width: sizeVal,
            display: 'inline-block',
            position: 'relative',
            verticalAlign: 'middle'
          }}>
            <span key={id+'box'} style={{
              display: 'block',
              height: sizeVal,
              width: sizeVal,
              boxSizing: 'border-box',
              borderRadius: 2,
              borderWidth: 2,
              borderStyle: 'solid',
              backgroundColor: disabled ? gray[500] : (checked ? red[500] : 'white'),
              borderColor: disabled ? gray[500] : (checked ? red[500] : gray[400]),
              cursor: 'pointer',
              transition: '.2s',
              pointerEvents: disabled && 'none',
              ...(Radium.getState(this.state, id+'-label', ':hover')) && boxHoverStyles,
              ...(Radium.getState(this.state, id+'-label', ':active')) && boxActiveStyles,
              ...(Radium.getState(this.state, id, ':focus')) && boxFocusStyles
            }}></span>
            <span key={id + 'check'} style={{
              display: 'block',
              position: 'absolute',
              top: sizeVal/12,
              left: sizeVal/3.42,
              width: sizeVal/4,
              height: sizeVal/2,
              borderRightWidth: 3,
              borderRightStyle: 'solid',
              borderRightColor: 'white',
              borderBottomWidth: 3,
              borderBottomStyle: 'solid',
              borderBottomColor: 'white',
              transform: checked ? 'scale(1) rotate(45deg)' : 'scale(0) rotate(45deg)',
              transition: '.2s',
              opacity: (Radium.getState(this.state, id+'-label', ':active')) ? '0.75' : '1',
              pointerEvents: 'none'
            }}></span>
          </span>
          {label && (
            <span style={{
              display: 'inline-block',
              verticalAlign: 'middle',
              lineHeight: sizeVal+'px',
              marginLeft: sizeVal/2,
              color: checked ? gray[900] : gray[800]
            }}>{label}</span>
          )}
        </label>
      </div>
    )
  }
}

import React, { Component } from 'react'
import Radium, { StyleRoot } from 'radium'
import { connect } from 'react-redux'
// import * as actions from '../utils/actions'

const mapStateToProps = (state, ownProps) => {
  return {
    windowSize: state.items
  }
}

@Radium
@connect(mapStateToProps)
export default class Forge extends Component {

  componentDidMount() {
    window.addEventListener('resize', () => {
      this.props.setWindowResize({width: window.innerWidth, height: window.innerHeight })
    })
    // window.addEventListener('scroll', () => {
    //   this.props.setWindowScroll({x: window.pageXOffset, y: window.pageYOffset })
    // })
  }

  componentWillUnmount() {
    window.removeEventListener('resize', () => {
      this.props.setWindowResize({width: window.innerWidth, height: window.innerHeight })
    }, true)
    window.removeEventListener('scroll', () => {
      this.props.setWindowScroll({x: window.pageXOffset, y: window.pageYOffset })
    }, true)
  }

  render({props: { children, style, level, hoverLevel, theme }} = this) {
    return (
      <div style={{...style}}>
        <StyleRoot>
          {children}
        </StyleRoot>
      </div>
    )
  }
}

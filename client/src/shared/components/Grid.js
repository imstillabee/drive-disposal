import React, { Component } from 'react'
import Radium from 'radium'
import { media } from '../styles/responsive'

const gutterWidth = 24
const marginWidth = 64

@Radium
export class Grid extends Component {
  render ({ props: {
    fluid,
    style,
    children
  } } = this) {
    return (
      <div style={{
        display: 'flex',
        flexDirection: 'column',
        marginTop: '0',
        marginRight: 'auto',
        marginBottom: '0',
        marginLeft: 'auto',
        maxWidth: !fluid && 1440,
        paddingTop: 0,
        paddingRight: marginWidth,
        paddingBottom: 0,
        paddingLeft: marginWidth,
        ...style,
        [`@media (max-width: ${media.xs}px)`]: { paddingTop: 0, paddingRight: 0, paddingBottom: 0, paddingLeft: 0 },
        [`@media (max-width: ${media.sm}px)`]: { paddingTop: 0, paddingRight: 0, paddingBottom: 0, paddingLeft: 0 }
      }}>{children}</div>
    )
  }
}

export const Row = ({
  children,
  style
}) =>
  <div style={{
    marginTop: '0',
    marginRight: -gutterWidth / 2,
    marginBottom: '0',
    marginLeft: -gutterWidth / 2,
    display: 'flex',
    flex: '1',
    flexWrap: 'wrap',
    ...style
  }}>{children}</div>


@Radium
export class Col extends Component {
  colWidth (num) { return parseInt(num, 10) / 12 * 100 + '%' }
  render ({
    props: {
      width,
      xsWidth,
      smWidth,
      mdWidth,
      lgWidth,
      xlWidth,
      offset,
      xsOffset,
      smOffset,
      mdOffset,
      lgOffset,
      xlOffset,
      style,
      children
    }, colWidth
  } = this) {
    return (
      <div style={{
        paddingTop: 0,
        paddingRight: gutterWidth / 2,
        paddingBottom: 0,
        paddingLeft: gutterWidth / 2,
        boxSizing: 'border-box',
        flexBasis: width && colWidth(width),
        maxWidth: width && colWidth(width),
        marginLeft: offset && colWidth(offset),
        ...style,
        [`@media (max-width: ${media.xs}px)`]: {
          flexBasis: xsWidth ? colWidth(xsWidth) : '100%',
          maxWidth: xsWidth ? colWidth(xsWidth) : '100%',
          marginLeft: xsOffset ? colWidth(xsOffset) : null
        },
        [`@media (max-width: ${media.sm}px)`]: {
          flexBasis: smWidth ? colWidth(smWidth) : '100%',
          maxWidth: smWidth ? colWidth(smWidth) : '100%',
          marginLeft: smOffset ? colWidth(smOffset) : null
        },
        [`@media (max-width: ${media.md}px)`]: {
          flexBasis: mdWidth ? colWidth(mdWidth) : lgWidth ? width : null,
          maxWidth: mdWidth ? colWidth(mdWidth) : lgWidth ? width : null,
          marginLeft: mdOffset ? colWidth(mdOffset) : null
        },
        [`@media (max-width: ${media.lg}px)`]: {
          flexBasis: lgWidth ? colWidth(lgWidth) : xlWidth ? width : null,
          maxWidth: lgWidth ? colWidth(lgWidth) : xlWidth ? width : null,
          marginLeft: lgOffset ? colWidth(lgOffset) : null
        },
        [`@media (max-width: ${media.xl}px)`]: {
          flexBasis: xlWidth ? colWidth(xlWidth) : null,
          maxWidth: xlWidth ? colWidth(xlWidth) : null,
          marginLeft: xlOffset ? colWidth(xlOffset) : null
        }
      }}>{children}</div>
    )
  }
}

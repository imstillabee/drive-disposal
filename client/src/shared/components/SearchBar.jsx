import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import Radium from 'radium'
import Button from './Button'
import Portal from './Portal'
import Icon from './Icon'
import { shadow } from '../styles/shadow'
import { gray } from '../styles/color'
import { Link, withRouter } from 'react-router-dom'

@withRouter
@Radium
export default class SearchBar extends Component {
  constructor() {
    super()
    this.state = {
      matches: [],
      query: '',
      showMatches: false,
      selectedMatch: 0
    }
  }

  componentDidMount() {
    const el = ReactDOM.findDOMNode(this.refs['SearchBarEl'])
    this.setState({searchBarEl: el, searchBarRect: el.getBoundingClientRect() })
    window.addEventListener('keydown', (e) => {
      if(this.state.matches.length && this.state.showMatches) {
        switch (e.keyCode) {
          case 38: //up
            e.preventDefault()
            this.setState({selectedMatch: this.state.selectedMatch > 0 ? this.state.selectedMatch - 1 : this.state.selectedMatch })
            break
          case 40: //down
            e.preventDefault()
            this.setState({selectedMatch: this.state.selectedMatch < this.state.matches.length - 1 ? this.state.selectedMatch + 1 : this.state.selectedMatch })
            break
          case 13: //enter
            e.preventDefault()
            this.props.history.push(this.state.matches[this.state.selectedMatch].link)
            this.setState({ query:'', showMatches: false })
            break
          default: return
        }
      }
    })
  }

  componentWillUnmount() {
    window.removeEventListener('keydown', this.handleKeyPress)
  }

  handleKeyPress = (e) => {}

  componentWillReceiveProps(newProps) {
    this.setState({matches: newProps.matches})
  }

  render ({props, state} = this) {
    const style = {
      wrap: {
        position: 'relative'
      },
      searchWrap: {
        width: '100%',
        display: 'flex',
        backgroundColor: '#E1E1E1',
        borderRadius: 40,
        transition: '200ms',
        ...Radium.getState(this.state, 'SearchBarInput', ':hover') && {
          backgroundColor: 'white',
          boxShadow: '0 0 0 1px rgba(0,0,0,0.1)'
        },
        ...Radium.getState(this.state, 'SearchBarInput', ':focus') && {
          backgroundColor: 'white',
          boxShadow: shadow[4]
        }
      },
      input: {
        flex: '1',
        fontSize: 14,
        padding: '8px 20px',
        height: 40,
        boxSizing: 'border-box',
        border: 0,
        background: 'transparent',
        outline: 'none',
        ':hover': {},
        ':focus': {}
      },
      button: {
        paddingLeft: 16,
        paddingRight: 16,
        height: 32,
        margin: 4
      },
      matchesWrap: {
        width: this.state.searchBarRect ? this.state.searchBarRect.width : '100%',
        position: 'fixed',
        top: this.state.searchBarRect && this.state.searchBarRect.y + this.state.searchBarRect.height + 4 ,
        left: this.state.searchBarRect && this.state.searchBarRect.x,
        backgroundColor: gray[50],
        boxShadow: shadow[4],
        paddingTop: 8,
        paddingBottom: 8
      },
      match: {
        fontSize: 15,
        padding: '8px 16px',
        lineHeight: '20px',
        transition: '200ms',
        cursor: 'pointer',
        color: this.props.theme === 'dark' ? gray[500] : gray[800],
        display: 'flex',
        alignItems: 'center',
        ':hover': {
          color: this.props.theme === 'dark' ? '#ffffff' : gray[800],
          backgroundColor: this.props.theme === 'dark' ? '#171717' : gray[200]
        },
        ':active': {
          backgroundColor: this.props.theme === 'dark' ? '#111111' : gray[300]
        }
      },
      selectedMatch: {
        color: this.props.theme === 'dark' ? '#ffffff' : gray[800],
        backgroundColor: this.props.theme === 'dark' ? '#171717' : gray[200]
      },
      matchSubtext: {
        fontSize: 12,
        display: 'block',
        opacity: '0.75'
      }
    }
    return (
      <div style={style.wrap}>
        <div ref="SearchBarEl" style={style.searchWrap}>
          <input
            key="SearchBarInput"
            type="text"
            placeholder={props.placeholder ? props.placeholder :'Search'}
            value={this.state.query}
            onFocus={(e)=> {state.query && this.setState({showMatches: true})}}
            autoFocus={props.autoFocus}
            onChange={
              (e) => {
                const el = ReactDOM.findDOMNode(this.refs['SearchBarEl'])
                this.setState({query: e.target.value, showMatches: true, searchBarEl: el, searchBarRect: el.getBoundingClientRect(), selectedMatch: 0 })
                props.onChange && props.onChange(e.target.value)
              }
            }
            style={style.input} />
          <Button icon="search" type="primary" style={style.button} />
        </div>
        <Portal open={(props.matches && props.matches.length && state.showMatches)}>
          <div style={{
            position: 'fixed',
            top: 0,
            right: 0,
            bottom: 0,
            left: 0,
            zIndex: 50
          }} onClick={ () => {this.setState({showMatches: false, selectedMatch: 0})} }>
            <div style={style.matchesWrap}>
              {state.matches.map((match, i) =>
                <Link
                  key={ `matchLink-${i}` }
                  to={ match.link }
                  style={{textDecoration: 'none'}}>
                  <div key={ `match-${i}`} style={{
                    ...style.match,
                    ...(this.state.selectedMatch === i) && style.selectedMatch
                  }} onClick={ ()=>{
                    this.setState({ query:'', showMatches: false })
                  }}>
                    {match.icon && (<Icon name="server" style={{marginRight: 16, opacity: .75}} />)}
                    <div>
                      <span>{ match.text }</span>
                      { match.subtext && (<span style={style.matchSubtext}>{ match.subtext }</span>)}
                    </div>
                  </div>
                </Link>
              )}
            </div>
          </div>
        </Portal>
      </div>
    )
  }
}

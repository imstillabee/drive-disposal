import React from 'react'

import icons from '../utils/getIcons'

export default function Icon (props) {
  let {color, size, name, style} = props

  let defaultStyle = {
    display: 'inline-block',
    height: size,
    width: size,
    color: color
  }
  return (
    <div style={{...defaultStyle, ...style}} dangerouslySetInnerHTML={{__html: icons[name]}}></div>
  )
}

Icon.defaultProps = {
  size: 24
}

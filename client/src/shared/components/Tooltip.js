import React, { Component } from 'react'
import Radium from 'radium'

@Radium
export default class Tooltip extends Component {
  render ({ props: { children, style, level, hoverLevel, theme } } = this) {
    return (
      <div style={ style }>
        {children}
      </div>
    )
  }
}

import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Grid, Row, Col } from '_forge/Grid'

class ObjectPrint extends Component {
  render () {
    const source = this.props.sourceObject || { Error: 'Error loading source object' }

    const properties = Object.getOwnPropertyNames(source)
    const olb = this.props.objectLabelMap

    properties.forEach(item => {
      switch (typeof source[item]) {
        case 'boolean':
          source[item] = source[item].toString()
          break
        default:
          break
      }
      const checkItem = item.toLowerCase()
      let isDate = false

      // Date Trigger Words
      const triggerWords = ['date', 'start', 'stop', 'end']
      triggerWords.forEach(word => {
        if (checkItem.includes(word)) isDate = true
      })

      if (isDate) {
        const theDate = new Date(source[item])
        source[item] = `${theDate.getMonth() + 1}-${theDate.getDate()}-${theDate.getFullYear()}`
      }
    })

    return (
      <Grid fluid>
        <Row>
          <Col width={ 12 }><h2 style={{ marginTop: 16, marginBottom: 24 }}>{ this.props.label }</h2></Col>
        </Row>
        <Row>
          { properties.filter(item => (
            typeof source[item] !== 'undefined' &&
            (
              typeof source[item] === 'string' ||
              typeof source[item] === 'number' ||
              typeof source[item] === 'boolean'
            )
          )).map(item => {
              let label = item.replace(/([A-Z])/g, ' $1').trim()
              if (olb && olb[item]) label = olb[item]
              return (
                <Col key={ `${item}-key` } width={ 3 }>
                  <div style={{ marginBottom: 4, wordWrap: 'break-word', textTransform: 'capitalize', fontSize: 13, color: '#828282', fontWeight: 'bold' }}>{ label }</div>
                  <div style={{ marginBottom: 24, wordWrap: 'break-word', fontWeight: 'bold', color: 'black' }}>{ source[item] }</div>
                </Col>
              )
          } ) }
        </Row>
      </Grid>
    )
  }
}

ObjectPrint.propTypes = {
  label: PropTypes.string.isRequired,
  sourceObject: PropTypes.object,
  objectLabelMap: PropTypes.object
}

ObjectPrint.defaults = {
  objectLabelMap: {}
}

export default ObjectPrint

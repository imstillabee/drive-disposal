import React, { Component } from 'react'
import { gray } from '../styles/color'
import Prefixer from 'inline-style-prefixer'
import Checkbox from './Checkbox'

const prefixer = new Prefixer()

export class Table extends Component {
  render({ props } = this) {
    const childrenWithProps = React.Children.map(props.children, (child) => {
      return React.cloneElement(child, {
        selection: props.selection,
        selectable: props.selectable,
        onSelect: props.onSelect,
        onSelectAll: props.onSelectAll,
        allSelected: props.allSelected
      })
    })

    const style = {
      table: {
        width: '100%',
        borderSpacing: 0,
      }
    }
    return (
      <table style={{...style.table, ...props.style}}>{childrenWithProps}</table>
    )
  }
}

export class TableHeader extends Component {
  render({ props } = this) {
    const childrenWithProps = React.Children.map(props.children, (child) => {
      return React.cloneElement(child, {
        context: 'TableHeader',
        selection: props.selection,
        selectable: props.selectable,
        onSelectAll: props.onSelectAll,
        allSelected: props.allSelected
      })
    })

    const style = {
      tableHeader: {}
    }
    return (
      <thead style={{...style.tableHeader, ...props.style}}>{childrenWithProps}</thead>
    )
  }
}

export class TableBody extends Component {
  render({ props } = this) {
    const childrenWithProps = React.Children.map(props.children, (child) => {
      return React.cloneElement(child, {
        context: 'TableBody',
        selection: props.selection,
        selected: props.selection.length ? props.selection.includes(child.key) : false,
        rowId: child.key,
        onSelect: props.onSelect,
        selectable: props.selectable
      })
    })

    const style = {
      tableBody: {}
    }
    return (
      <tbody style={{...style.tableBody, ...props.style}}>{childrenWithProps}</tbody>
    )
  }
}

export class TableFooter extends Component {
  render({ props } = this) {
    const childrenWithProps = React.Children.map(props.children, (child) => {
      return React.cloneElement(child, { context: 'TableFooter', selection: props.selection })
    })

    const style = {
      tableFooter: {}
    }
    return (
      <tfoot style={{...style.tableFooter, ...props.style}}>{childrenWithProps}</tfoot>
    )
  }
}

export class TableRow extends Component {
  render({ props } = this) {

    const childrenWithProps = React.Children.map(props.children, (child, i) => {
      return React.cloneElement(child, {
        context: props.context,
        firstChild: i === 0,
        lastChild: i === props.children.length-1,
        selected: props.selected,
        onSelectAll: props.onSelectAll
      })
    })

    const style = {
      tableRow: {},
      endCap: {
        width: 64,
        paddingTop: 0,
        paddingRight: 0,
        paddingBottom: 0,
        paddingLeft: 0,
        textAlign: 'center'
      }
    }
    return (
      <tr style={{...style.tableRow, ...props.style}}>
        <TableCell selected={ props.selected } context={props.context} style={{...style.endCap}}>
          {props.selectable && props.context === 'TableBody' && (<Checkbox size="sm" checked={props.selected} onClick={() => { props.onSelect(props.rowId) }} />)}
          {props.selectable && props.context === 'TableHeader' && (<Checkbox size="sm" checked={props.allSelected} onClick={props.onSelectAll && props.onSelectAll} />)}
        </TableCell>
        {childrenWithProps}
        <TableCell selected={ props.selected } context={props.context} style={{...style.endCap}}>

        </TableCell>
      </tr>
    )
  }
}

export class TableCell extends Component {
  render({ props } = this) {
    const style = {
      tableCell: {
        paddingTop: 12,
        paddingRight: 24,
        paddingBottom: 12,
        paddingLeft: 0,
        borderBottomWidth: 1,
        borderBottomColor: gray[200],
        borderBottomStyle: 'solid',
        fontSize: 14,
        lineHeight: '24px',
        transition: '200ms',
        ...(props.context === 'TableHeader' || props.context === 'TableFooter') && {
          color: gray[500],
          fontWeight: 'bold'
        },
        ...(props.context === 'TableHeader') && prefixer.prefix({
          position: 'sticky',
          top: 104,
          backgroundColor: 'white',
          zIndex: 1
        }),
        ...(props.context === 'TableFooter') && prefixer.prefix({
          position: 'sticky',
          bottom: 0,
          backgroundColor: 'white',
          borderBottomWidth: 0,
          borderBottomColor: 'transparent',
          borderBottomStyle: 'none',
          borderTopWidth: 1,
          borderTopColor: gray[200],
          borderTopStyle: 'solid',
          zIndex: 1
        }),
        ...(props.selected) && { backgroundColor: gray[50] },
        ...props.style
        // ...(props.firstChild) && { paddingLeft: 64 },
        // ...(props.lastChild) && { paddingRight: 64 }
      }
    }
    return (
      <td colSpan={props.colSpan} style={{...style.tableCell, ...props.style}}>{props.children}</td>
    )
  }
}

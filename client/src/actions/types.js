export const FETCH_USER = 'fetch_user';
export const FETCH_DRIVES = 'fetch_drives';
export const FETCH_SITES = 'fetch_sites';
export const CREATE_SITES = 'create_sites';


export const LOAD_SITES_SUCCESS = 'load_sites_success'
export const CREATE_DRIVE_SUCCESS = 'create_drive_success'
export const LOAD_DRIVES_SUCCESS = 'load_drives_success'

export const AUTH_USER = 'auth_user',
             UNAUTH_USER = 'unauth_user',
             AUTH_ERROR = 'auth_error',
             FETCH_HOST = 'fetch_host'
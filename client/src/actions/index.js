import axios from 'axios';
import https from 'https';
import { FETCH_USER, 
  FETCH_DRIVES, 
  FETCH_SITES, 
  AUTH_USER,  
  AUTH_ERROR,
  UNAUTH_USER,
  CREATE_SITES,
FETCH_HOST } from './types';

const SSO_API_URL = 'https://sso.verizoncloudplatform.com/authenticate'
  
//   export const errorHandler = (dispatch, error, type) => {
//   let errorMessage = '';

//   if(error.data.error) {
//     errorMessage = error.data.error;
//   } else if(error.data) {
//     errorMessage = error.data;
//   } else {
//     errorMessage = error;
//   }

//   if(error.status === 401) {
//     dispatch({
//       type: type,
//       payload: 'You are not authorized to do this. Please login and try again.'
//     });
//     logoutUser();
//   } else {
//     dispatch({
//       type: type,
//       payload: errorMessage
//     });
//   }
// }

//   export const logoutUser = (error) => {
//   return function (dispatch) {
//     dispatch({ type: UNAUTH_USER, payload: error || '' });
//     cookie.remove('token', { path: '/dd' });
//     cookie.remove('user', { path: '/' });
//     window.location.href = '/dd/login';
//   }
// }

// export const protectedTest = () => {
//   return function(dispatch) {
//     axios.get('/api/protected', {
//       headers: { Authorization: cookie.load('token') }
//     })
//     .then((response) => {
//       dispatch({
//           type: PROTECTED_TEST,
//           payload: response.data.content
//         });
//     })
//     .catch((error) => {
//       errorHandler(dispatch, error.response, AUTH_ERROR)
//     })
//   }
// }


// export const fetchUser = (user) => async dispatch => {
//     let token = cookie.load('token')
//     const res = await axios.get(`/api/login`, {
//       headers: { 'Authorization': `Bearer ${token}`}
//     });
//     cookie.save('user', res.data.user, { path: '/dd' });
//     dispatch({type: FETCH_USER});
// };
  
  export function loginUser({ username, password }){
    const agent = new https.Agent({ family: 4 });
    return function(dispatch) {
      //Submit user/password to the server
      axios({ method: 'POST', url: `${SSO_API_URL}`, data: { 
        username, 
        password,
        membership: 'CN=special-users,OU=Groups,DN=example,DN=tld',
        next: "moss.verizonwireless.com"
        }, httpsAgent: agent})
        .then(response => {
          //If req is good...
          // - Update state to indicate that user is auth'd
          dispatch({ type: AUTH_USER});
          // - Save JWT token
          localStorage.setItem('token', response.data.token);
          // - redirect to the route /sites
          window.location.href = '/dd';
        })
        .catch(() => {
          dispatch(authError('Bad Login Info'));
        })
    }
  }

  export const authError = (error) => {
    return {
      type: AUTH_ERROR,
      payload: error
    }
  }

  export const logoutUser = () => {
    localStorage.removeItem('token');
    return { type: UNAUTH_USER }
  }

  // export function parseJwt (token) {
  //           var base64Url = token.split('.')[1];
  //           var base64 = base64Url.replace('-', '+').replace('_', '/');
  //           return {
  //             type: FETCH_USER,
  //             payload: JSON.parse(window.atob(base64))
  //           }
  //       };

  export const fetchUser = () => async dispatch => {
    let token = localStorage.getItem('token')
    const res = await axios.get(`https://dev.moss.verizoncloudplatform.com/v1/auth`, {
      headers: { 'Authorization': `Bearer ${token}`}
    });
    dispatch({type: FETCH_USER, payload: res.data});
  }

export const fetchHost = (hostName, sn) => async dispatch => {
    let token = localStorage.getItem('token')
    const res = await axios.get(`https://dev.moss.verizoncloudplatform.com/v1/host/find/${hostName}/${sn}`, {
       headers: { 'Authorization': `Bearer ${token}`}
    });
    dispatch({type: FETCH_HOST, payload: res.data});
};

export const submitDrive = (values, history) => async dispatch => {
  const res = await axios.post('/api/drives', values, {
      headers: { authorization: localStorage.getItem('token') }
    });

  history.push('/dd');
  dispatch({type: FETCH_USER, payload: res.data});
};

export const submitSite = (values, history) => async dispatch => {
  const res = await axios.post('/api/site', values, {
      headers: { authorization: localStorage.getItem('token') }
    });

  history.push(`/dd/drives/${values.name}`);
  dispatch({type: CREATE_SITES, payload: res.data});
};

export const fetchSiteLocation = (location) => async dispatch => {
  const res = await axios.get(`/api/sites/${location}`, {
      headers: { authorization: localStorage.getItem('token') }
    });

  dispatch({ type: FETCH_SITES, payload: res.data});
};

export const fetchDrives = () => async dispatch => {
  const res = await axios.get('/api/drives', {
      headers: { authorization: localStorage.getItem('token') }
    });

  dispatch({ type: FETCH_DRIVES, payload: res.data});
};

export const fetchDriveLocations = (location) => async dispatch => {
  const res = await axios.get(`/api/drives/${location}`, {
      headers: { authorization: localStorage.getItem('token') }
    });

  dispatch({ type: FETCH_DRIVES, payload: res.data});
};

export const deleteDrive = (id, history) => async dispatch => {
  let { data } = await axios.delete(`/api/drives/delete/${id}`, {
      headers: { authorization: localStorage.getItem('token') }
    });

  window.location.reload();
  dispatch({ type: FETCH_DRIVES, payload: data });
};

export const deleteSite = (id, history) => async dispatch => {
  let { data } = await axios.delete(`/api/sites/delete/${id}`, {
      headers: { authorization: localStorage.getItem('token') }
    });

  history.pushState(null, '/dd/addsite');
  dispatch({ type: FETCH_SITES, payload: data });
};

export const fetchSites = () => async dispatch => {
  const res = await axios.get('/api/sites', {
      headers: { authorization: localStorage.getItem('token') }
    });

  dispatch({ type: FETCH_SITES, payload: res.data});
};
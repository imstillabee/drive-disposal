export default {
  app: {
    id: 'operations',
    title: 'Operations'
  },
  modules: [
    // {
    //   exact: true,
    //   path: '/',
    //   component: Dashboard,
    //   title: 'Dashboard',
    //   icon: 'dashboard'
    // },
    {
      exact: true,
      path: '/',
      title: 'Hostname Search',
      showInNav: true,
      icon: 'search',
      hideSearchBar: true,
      hideTitleBar: true,
      // appBarCollapsed: true,
      appBarTransparent: true
    },
    {
      path: '/host/:hostName/:serialNumber',
      title: 'Details',
      icon: 'page-text',
      hasTabs: true,
    },
    {
      path: '/sites',
      title: 'Sites',
      icon: 'circle-outlined-pin',
      showInNav: true,
    },
    {
      path: '/procedures',
      title: 'Procedures',
      icon: 'clipboard',
      showInNav: true
    },
    {
      path: '/reference',
      title: 'Reference',
      icon: 'circle-outlined-info'
    },
    {
      path: '/playground',
      title: 'Playground',
      icon: 'wrench'
    },
    {
      path: '/login',
      title: 'Login',
      icon: 'user',
      hideAppBar: true
    }
  ]
}

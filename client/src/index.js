
import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux';
import { createStore, applyMiddleware} from 'redux';
import { BrowserRouter as Router, Route } from 'react-router-dom'
import reduxThunk from 'redux-thunk';
import './index.css'
import DriveList from './components/Drives/DriveList'
import AddDrive from './components/Drives/AddDrive'
import AddSite from './components/Sites/AddSite'
import SiteList from './components/Sites/SiteList';
import Login from './components/Auth/login'
import Logout from './components/Auth/logout'
import { PrivateRoute } from './components/Auth/requireAuth'
import reducers from './reducers';
import { AUTH_USER } from './actions/types';
import Forge from '_forge/Forge';

const createStoreWithMiddleware = applyMiddleware(reduxThunk)(createStore);
const store = createStoreWithMiddleware(reducers);
const token = localStorage.getItem('token');

if (token) {
  store.dispatch({ type: AUTH_USER});
}


ReactDOM.render(
  <Provider store={store}>
    <Forge>
      <Router>
        <div>
          <div>
            <Route path="/dd/login" component={Login} />
            <Route path="/dd/logout" component={Logout} />
            <Route exact path="/dd" component={SiteList} />
            <PrivateRoute path="/dd/drives/:name" component={DriveList} />
            <PrivateRoute path="/dd/add" component={AddDrive} />
            <PrivateRoute path="/dd/addsite" component={AddSite} />
          </div>
        </div>
      </Router>
    </Forge>
  </Provider>,
  document.querySelector('#root'));

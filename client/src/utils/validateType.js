const re = /^(HDD|SSD)$/;

export default (type) => {
    const invalidTypes = type
        .split(',')
        .map(type => type.trim())
        .filter(type => re.test(type) === false)

        if (invalidTypes.length) {
            return "Please enter one of the vaild types: HDD or SSD";
        }

        return;
};
const re = /^(Dell|HP|Cisco|Toshiba|Seagate|HGST|Intel)$/;

export default (manu) => {
    const invalidManufacturer = manu
        .split(',')
        .map(manu => manu.trim())
        .filter(manu => re.test(manu) === false)

        if (invalidManufacturer.length) {
            return "This is not a valid Manufacturer";
        }

        return;
};
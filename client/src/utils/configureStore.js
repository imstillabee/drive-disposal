import { createStore, applyMiddleware, compose } from 'redux'
import combineReducers from '../reducers'
import thunk from 'redux-thunk'
import promiseMiddleware from 'redux-promise'

export default function configureStore (initialState) {
  const composeEnhancers =
    typeof window === 'object' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
      window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({}) : compose

  return createStore(
    combineReducers,
    initialState,
    composeEnhancers(
      applyMiddleware(
        thunk,
        promiseMiddleware
      )
    )
  )
}

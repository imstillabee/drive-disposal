const easternTime = ["ME","MA","RI","CT","NJ","DE","MD","NH","VT","NY","PA","VA","WV","OH","MI","IN","KY","NC","SC","GA","FL","ON","Colombia","Equador","Peru"];

const centralTime = ["TX","LA","MS","AL","TN","AR","OK","KS","MO","IL","IA","NE","ND","SD","MN","WI","Mexico"];

const mountianTime = ["NM","CO","WY","MT","ID","UT","AZ"];

const pacificTime = ["WA","OR","NV","CA","BC"];

const westEUTime = ["Ireland","Scotland","UK","Portugal","Iceland"];

const centralEUTime = ["Spain","France","Belgium","Germany","Poland","Italy","Monaco","Switzerland","Czech Republic","Austria","Slovakia","Hungary","Croatia","Bosnia","Poland","Denmark","Netherlands","Norway","Sweden"];

const eastEUTime = ["Romainia","Bulgaria","Greece","Ukraine","Lithuania","Latvia","Estonia","Finland"];

const centralSouthTime = ["Venzuela","Guyana","Chile","Bolivia","Paraguay"];

const eastSouthTime = ["Argentina","Uruguay","Brazil"];

export default (state) => {
        if (easternTime.indexOf(state) >= 0) {
            return "New_York";
        } else if (centralTime.indexOf(state) >= 0) {
            return "Chicago";
        } else if (mountianTime.indexOf(state) >= 0) {
            return "Denver";
        } else if (pacificTime.indexOf(state) >= 0) {
            return "Los_Angeles";
        } else if (westEUTime.indexOf(state) >= 0) {
            return "Dublin";
        } else if (centralEUTime.indexOf(state) >= 0) {
            return "Madrid";
        } else if (eastEUTime.indexOf(state) >= 0) {
            return "Sofia";
        } else if (centralSouthTime.indexOf(state) >= 0) {
            return "Guyana";
        } else if (eastSouthTime.indexOf(state) >= 0) {
            return "Sao_Paulo";
        } else if (state === "India") {
            return "Kathmandu";
        } else if ((state === "China") || (state === "Singapore")) {
            return "Hong_Kong";
        } else if (state === "Japan") {
            return "Tokyo";
        } else if (state === "Australia") {
            return "Sydney";
        }
};
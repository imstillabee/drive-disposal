const re = /^(Amsterdam|Arlington|Atlanta|Bloomington|Branchburg|Buffalo|Charlton|Cologne|Copenhagen|Frankfurt|Hong Kong|Colorado Springs|Harrisburg|Kent|Pittsburgh|Reston|Riverdale|Rocklin|San Jose|Temple Terrace|Twinsburg|London|Madrid|Mexico City|Milan|Mumbai|Paris|Santiago|Sao Paulo|Singapore|Stockholm|Sydney|TokyoToronto|Torrance|Vancouver|Southlake)$/;

export default (location) => {
    const invalidSite = location
        .split(',')
        .map(location => location.trim())
        .filter(location => re.test(location) === false)

        if (invalidSite.length) {
            return "This is not a valid Cloud Site";
        }

        return;
};
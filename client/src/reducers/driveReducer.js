import { FETCH_DRIVES } from '../actions/types';

export default function(state = [], action) {
    switch (action.type) {
        case FETCH_DRIVES:
            return action.payload;
        default: 
            return state;
    }
}
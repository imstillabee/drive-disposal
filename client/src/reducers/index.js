import {combineReducers} from 'redux';
import { reducer as reduxForm } from 'redux-form';
import authReducer from './authReducer';
import driveReducer from './driveReducer';
import siteReducer from './siteReducer';
import hostReducer from './hostReducer';
import userReducer from './userReducer';


export default combineReducers({
  drives: driveReducer,
  sites: siteReducer,
  auth: authReducer,
  form: reduxForm,
  host: hostReducer,
  user: userReducer
});

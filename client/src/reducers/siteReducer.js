import { FETCH_SITES, CREATE_SITES } from '../actions/types';

const addSiteToState = (sites, state) => {
  return sites.reduce((result, site) => {
    if (!site) return result;
    return {
      ...result,
      [site.id]: {
        ...result[site.id],
        ...site
      }
    };
  }, state);
}

export default function(state = [], action) {
    switch (action.type) {
        case FETCH_SITES:
            return action.payload;
        case CREATE_SITES:
            return addSiteToState([action.payload], state);
        default: 
            return state;
    }

}

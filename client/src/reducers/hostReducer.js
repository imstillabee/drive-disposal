import { FETCH_HOST } from '../actions/types';

export default function(state = [], action) {
    switch (action.type) {
        case FETCH_HOST:
            return action.payload;
        default: 
            return state;
    }
}
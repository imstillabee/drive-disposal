import React from 'react'

const ErrorDisplay = ({ errors }) => {
  return (
    <div className='error'>
      <p>{errors}</p>
    </div>
  )
}

ErrorDisplay.propTypes = {
  errors: React.PropTypes.string
}

export default ErrorDisplay

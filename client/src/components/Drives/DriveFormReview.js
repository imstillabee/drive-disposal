//DriveFromReview shows users thier form inputs for review
import _ from 'lodash';
import React from 'react';
import { connect } from 'react-redux';
import formFields from './formFields';
import selectFields from './selectFields';
import dropdownFields from './dropdownFields';
import { withRouter } from 'react-router-dom';
import * as actions from '../../actions';
import PropTypes from 'prop-types'
import { Row, Col } from '_forge/Grid';
import Button from '_forge/Button';

const DriveReview = ({ onCancel, formValues, submitDrive, history, auth }) => {
    
    const reviewFields = _.map(formFields, ({ name, label }) => {
        return (
            <div key={name}>
                <label>{label}</label>
                <div>
                    {formValues[name]}
                </div>
            </div>
        );
    });

    const reviewSelect = _.map(selectFields, ({ name, label }) => {
        return (
            <div key={name}>
                <label>{label}</label>
                <div>
                    {formValues[name]}
                </div>
            </div>
        );
    });

    const reviewDrop = _.map(dropdownFields, ({ name, label }) => {
        return (
            <div key={name}>
                <label>{label}</label>
                <div>
                    {formValues[name]}
                </div>
            </div>
        );
    });

    return (
        <div className="container">
            <h2 style={{textAlign: 'center', paddingLeft: 20, paddingRight: 20}}>Please confirm your entries</h2>
            <Row>
                <Col width={1}></Col>
                <Col width={11}>
                    {reviewFields}
                </Col>
            </Row>
            <Row>
                <Col width={1}></Col>
                <Col width={11}>
                    {reviewSelect}
                </Col>
            </Row>
            <Row>
                <Col width={1}></Col>
                <Col width={11}>
                    {reviewDrop}
                </Col>
            </Row>
            <Col width={ 12 } style={{marginBottom: 12, marginTop: 20 }}>
                <Button size='lg' type='primary' submit style={{ width: '100%' }} onClick={() => submitDrive(formValues, history)}>Submit</Button>
            </Col>
            <Col width={12}>
                <a onClick={onCancel} style={{textAlign: 'center', margin: '10px auto', display: 'block'}}>
                    Cancel
                </a>
            </Col>
        </div>
    );
};

function mapStateToProps(state) {
    console.log(state);
    return { formValues: state.form.driveForm.values };
}

export default connect(mapStateToProps, actions)(withRouter(DriveReview));
export default [
    { label: 'Serial Number', name: 'serialNumber', noValueError: 'You must provide a Serial Number' },
    { label: 'Drive Type', name: 'driveType', noValueError: 'You must specify a Drive type of HDD or SSD' },
    { label: 'Recipient List', name: 'recipients' }
];
// DriveForm shows a form for a user to add input
import _ from 'lodash';
import React, { Component } from 'react';
import { reduxForm, Field } from 'redux-form';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { Link } from 'react-router-dom';
import validateEmails from '../../utils/validateEmails';
import validateTypes from '../../utils/validateType';
import validateSites from '../../utils/validateSites';
import validateManu from '../../utils/validateManufacturer'
import formFields from './formFields';
import selectFields from './selectFields';
import dropdownFields from './dropdownFields';
import { Row, Col } from '_forge/Grid';
import MenuItem from 'material-ui/MenuItem'
import Button from '_forge/Button';
import {
  SelectField,
  TextField
} from 'redux-form-material-ui'
class DriveForm extends Component {


    renderFields() {
        return _.map(formFields, ({ label, name }) => {
            return (
                        <Field 
                            key={name} 
                            component={TextField} 
                            type="text" 
                            label={label} 
                            name={name} 
                            hintText={label}
                            style={{display: 'block', marginBottom: 20, width: '100%'}}
                        /> 
            );
        });
    }

renderSelectOptions = (options) => (
    <MenuItem key={options.key} value={options.value} primaryText={options.value}/>
  )
    renderSelect() {
        return _.map(selectFields, ({ label, name, options }) => {
            console.log(options);
            return (
                        <Field 
                        key={name}
                        component={SelectField} 
                        type="select"
                        hintText={label}
                        name={name}
                        style={{display: 'block', marginBottom: 20, width: '100%'}}>
                            {options.map(this.renderSelectOptions)}
                        </Field>
            );
        });
    }

    renderDrop() {
        return _.map(dropdownFields, ({ label, name, options }) => {
            console.log(options);
            return (

                        <Field 
                        key={name}
                        component={SelectField} 
                        type="select"
                        hintText={label}
                        name={name}
                        style={{display: 'block', marginBottom: 20, width: '100%'}}>
                            {options.map(this.renderSelectOptions)}
                        </Field>
            );
        });
    }
    
    render() {
        return (
            <MuiThemeProvider muiTheme={getMuiTheme()}>
                <form onSubmit={this.props.handleSubmit(this.props.onSurveySubmit)}> {/*only call after user has submitted, not on render*/}
                    <h5 style={{textAlign: 'center', paddingLeft: 20, paddingRight: 20}}>Please include recipient(s) to verify the serial number</h5>
                    <Row>
                        <Col width={2}></Col>
                        <Col width={8}>
                            {this.renderFields()}
                        </Col>
                    </Row>
                    <Row>
                        <Col width={2}></Col>
                        <Col width={8}>
                            {this.renderSelect()}
                        </Col>
                    </Row>
                    <Row>
                        <Col width={2}></Col>
                        <Col width={8}>
                            {this.renderDrop()}
                        </Col>
                    </Row>
                    <Col width={ 12 } style={{marginBottom: 12 }}>
                        <Button size='lg' type='primary' submit style={{ width: '100%' }}>NEXT</Button>
                    </Col>
                    <Col width={12}>
                        <Link to="/" style={{textAlign: 'center', margin: '10px auto', display: 'block'}}>
                            Cancel
                        </Link>
                    </Col>
                </form>
            </MuiThemeProvider>
        );
    }
}

function validate(values) {

    const errors = {};

    errors.recipients = validateEmails(values.recipients || '');

    errors.driveType = validateTypes(values.driveType || '');

    errors.manufacturer = validateManu(values.manufacturer || '');

    errors.location = validateSites(values.location || '');

    _.each(formFields, selectFields, dropdownFields, ({ name, noValueError }) => {
        if (!values[name]) {
            errors[name] = noValueError;
        }
    });

    return errors;
}
    

export default reduxForm({
    validate,
    form: 'driveForm',
    destroyOnUnmount: false //keep form values
})(DriveForm);
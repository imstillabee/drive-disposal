// DriveNew shows DriveForm and FormReview
import React, { Component } from 'react';
import { reduxForm } from 'redux-form';
import DriveForm from './DriveForm';
import DriveFormReview from './DriveFormReview';
import AppAppBar from '_forge/AppAppBar';
import Card from '_forge/Card';
import { Grid, Row, Col } from '_forge/Grid';

class AddDrive extends Component {

    state = { showDriveReview: false };

    constructor(props) {
        super(props)
        this.state = {
            title: 'Manually Enter Disposed Drive',
            showDriveReview: false
        }
    }

    renderContent() {
        if (this.state.showDriveReview) {
            return <DriveFormReview
                onCancel={() => this.setState({showDriveReview: false })}    
            />;
        }

        return (
            <DriveForm
                onSurveySubmit={() => this.setState({ showDriveReview: true})}
            />
        );
    }
    render() {
        
        const style = {
            card: {
                'default': {
                    position: 'relative',
                    boxSizing: 'border-box',
                    overflow: 'hidden',
                    marginBottom: '30px'
                    }
                }
        }

        const { title } = this.state
        return (
            <div>
                <AppAppBar currentRoute={ title } />
                <Grid style={{ paddingTop: 0, paddingRight: 0, paddingBottom: 0, paddingLeft: 0 }}>
                    <Row style={{
                minHeight: '100vh',
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
                marginTop: 30
            }}>
                        <Col width={9}>
                            <Card style={{...style.card.default}}>
                                <Row>
                                    <Col width={12}>
                                        {this.renderContent()}
                                    </Col>
                                </Row>
                            </Card>
                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}

export default reduxForm({
    form: 'driveForm'
})(AddDrive);
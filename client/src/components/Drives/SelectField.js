import React, { Component } from 'react';
import PropTypes from 'prop-types';

class SelectField extends Component { // eslint-disable-line react/prefer-stateless-function

  renderSelectOptions = (options) => (
    <option key={options.key} value={options.value}>{options.value}</option>
  )

  render() {
    const { input, label, meta: {error, touched } } = this.props;
    return (
      <div>
        <label htmlFor={label}>{label}</label>
        <select {...input}>
          <option value="">Select</option>
          {this.props.options.map(this.renderSelectOptions)}
        </select>
        <div className="red-text" style={{ marginBottom: '20px' }}>
            {touched && error}
        </div>
      </div>
    );
  }
}

SelectField.propTypes = {
  options: PropTypes.array,
  input: PropTypes.object,
  label: PropTypes.string,
};

export default SelectField;
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types'
import { connect } from 'react-redux';
import { fetchDriveLocations, deleteDrive, fetchUser } from '../../actions';
import LineComparison from '../../shared/components/LineComparison';
import { LineChart, Line, CartesianGrid, XAxis, YAxis, Tooltip, Legend, ResponsiveContainer } from 'recharts';
import { withRouter } from 'react-router';
import _ from 'lodash';
import FuzzySearch from 'fuzzy-search';
import FilterBar from '_forge/FilterBar';
import AppAppBar from '_forge/AppAppBar';
import Button from '_forge/Button';
import Card from '_forge/Card';
import { Grid, Row, Col } from '_forge/Grid';
import moment from 'moment-timezone';
import timezone from '../../utils/timezone';
import Icon from '_forge/Icon'
import SiteInfo from '../Sites/SiteInfo'

class DriveList extends Component {

    static propTypes = {
        match: PropTypes.object.isRequired,
        location: PropTypes.object.isRequired,
        history: PropTypes.object.isRequired
    }

    constructor(props) {
        super(props)
        this.state = {
            brand: 'Dell',
            title: this.props.match.params.name + ' Current Drives',
            single: '',
            drives: this.props.drives || [],
            filterQuery: '',
            locationParam: this.props.match.params.name
        }
    }

    componentWillMount() {
        this.props.fetchUser();
    }
    
    componentDidMount() {
        this.props.fetchDriveLocations(this.props.match.params.name).then(() => {
            this.setState({drives: this.props.drives}, this.handleFilterChange(this.state.filterQuery));
        });
        setTimeout(function() { this.setCity(); }.bind(this), 6000);
        setTimeout(function() { this.renderAdd(); }.bind(this), 6000);
    }


      setCity() {
      if (this.props.user.profile.locations[0].city !== 'All') {
        this.setState((state) => ({
            single: this.props.auth.profile.locations[0].city
        }))
      } else {
          this.setState((state) => ({
            single: 'All'
        }))
      }
  }

        renderAdd() {
        if ((this.state.single === 'All') || (this.state.single === this.props.match.params.name)) {
            return <Link to="/dd/add"><Button size='lg' type='primary' submit style={{ width: '50%', display: 'block', margin: '0 auto 20px'}}>Add Drive to Site</Button></Link>
      } else {
        return;
      }
  }

    renderDelete() {
    switch (this.props.user) {
      case null:
        return;
      case false:
        return;
      default:
        return <span><Icon name="close" style={{color: '#B51C10'}} /></span>
    }
  }

  setCategory (category) {
    this.setState((state) => ({
      brand: category
    }));
    console.log(this.state.brand);
  }

    renderTime(time) {
      const country = timezone(time.state);
      if ((time.region === 'US') || (time.region === 'North America') || (time.region === 'South America')) {
         return moment().tz(`America/` + country).format('hh:MM A')
      } else {
        return moment().tz(`${time.region}/` + country).format('hh:MM A')
      }
  }

    handleFilterChange = (query) => {
    const searcher = new FuzzySearch(this.props.drives, ['serialNumber', 'driveType', 'manufacturer'])
    if(query.length) {
      this.setState({ drives: searcher.search(query), filterQuery: query })
    } else (
      this.setState({ drives: this.props.drives, filterQuery: '' })
    )
  }

    renderDrives() {
        const style = {
        card: {
            'default': {
                position: 'relative',
                boxSizing: 'border-box',
                overflow: 'hidden',
                marginBottom: '30px'
                }
            },
        cardLabel: {
            'default': {
                position:'relative',
                boxSizing: 'border-box',
                width:'10%',
                display: 'inline-block',
                float: 'left',
                background: '#ADADAD',
                marginLeft: '0'
            },
            'accessFull' : {
                position:'relative',
                boxSizing: 'border-box',
                width:'10%',
                display: 'inline-block',
                float: 'left',
                background: '#D52B1E',
                marginLeft: '0'
            },
            'accessLite' : {
                position:'relative',
                boxSizing: 'border-box',
                width:'10%',
                display: 'inline-block',
                float: 'left',
                background: '#00AC3E',
                marginLeft: '0'
            }
        }
        }
        return this.state.drives.map(drive => {
            return (


                <Col width={3} key={drive._id}>
                    <Card style={{...style.card.default}}>
                        <Row>
                            <Col width={2} offset={1} style={{...style.cardLabel.default}} >
                                <Icon name="device-phone" style={{marginRight: 16, opacity: .75, color: '#016726', display: 'block', margin: '100% auto', paddingLeft: '10px'}} />
                            </Col>
                            <Col width={9} style={{paddingLeft: '20px'}}>
                                <h4 style={{marginTop: 20, marginBottom: 0, display: 'inline-block'}}>{drive.serialNumber}</h4>
                                <a onClick={() => this.props.deleteDrive(drive._id)} style={{display: 'block', float: 'right', marginTop: 20, cursor: 'pointer'}}>
                                    {this.props.match.params.name === this.state.single ? this.renderDelete() : ''}
                                    {this.state.single === 'All' ? this.renderDelete() : ''}
                                </a>
                                <label style={{display: 'block'}}><b>Drive Type:</b> {drive.driveType}</label>
                                <label style={{display: 'block'}}><b>Drive Manufacturer:</b> {drive.manufacturer}</label>
                                <label style={{display: 'block'}}><b>Deposit Date:</b> {new Date(drive.depositDate).toLocaleDateString()}</label>
                                <label style={{display: 'block'}}><b>Vendor Pickup Date:</b> None</label>
                                <label style={{display: 'block'}}><b>Certificate of Destruction:</b> None</label>
                                
                            </Col>
                        </Row>
                        <Row>
                                {drive.yes >= 1 &&
                                    <label style={{display: 'block', width: '100%', padding: '0px 12px', background: '#016726', color: 'white'}}>
                                        <Icon style={{verticalAlign: 'middle', marginLeft: 10}} name="check" />
                                        <span style={{verticalAlign: 'middle', fontSize: 14, float: 'right', marginRight: 10, letterSpacing: 1, width: '80%', overflow: 'hidden', textOverflow: 'ellipsis'}}>{drive.recipients[0].email}</span> 
                                    </label>
                                    }
                                {drive.no >= 1 &&
                                <label style={{display: 'block', width: '100%', padding: '0px 12px', background: '#B51C10', color: 'white'}}>
                                    <Icon style={{verticalAlign: 'middle', marginLeft: 10}} name="close" />
                                    <span style={{verticalAlign: 'middle', fontSize: 14, float: 'right', marginRight: 10, letterSpacing: 1, width: '80%', overflow: 'hidden', textOverflow: 'ellipsis'}}>{drive.recipients[0].email}</span> 
                                </label>
                                }
                                {(drive.no < 1) && (drive.yes < 1) &&
                                <label style={{display: 'block', width: '100%', padding: '0px 12px', background: '#ADADAD', color: 'white'}}>
                                    <Icon style={{verticalAlign: 'middle', marginLeft: 10}} name="circle" />
                                    <span style={{verticalAlign: 'middle', fontSize: 14, float: 'right', marginRight: 10, letterSpacing: 1, width: '80%', overflow: 'hidden', textOverflow: 'ellipsis'}}>No Verification</span> 
                                </label>
                                }
                        </Row>
                    </Card>
                </Col>
            )
        })
    }
    
    render({props} = this) {

        const style = {
            card: {
                'default': {
                    position: 'relative',
                    boxSizing: 'border-box',
                    overflow: 'hidden',
                    marginBottom: '30px'
                    }
                },
            cardLabel: {
                'default': {
                    position:'relative',
                    boxSizing: 'border-box',
                    width:'10%',
                    display: 'inline-block',
                    float: 'left',
                    background: '#ADADAD',
                    marginLeft: '0'
                },
                'accessFull' : {
                    position:'relative',
                    boxSizing: 'border-box',
                    width:'10%',
                    display: 'inline-block',
                    float: 'left',
                    background: '#D52B1E',
                    marginLeft: '0'
                },
                'accessLite' : {
                    position:'relative',
                    boxSizing: 'border-box',
                    width:'10%',
                    display: 'inline-block',
                    float: 'left',
                    background: '#00AC3E',
                    marginLeft: '0'
                },
                'siteInfo' : {
                    position:'relative',
                    boxSizing: 'border-box',
                    width:'10%',
                    display: 'inline-block',
                    float: 'left',
                    background: '#025FCA',
                    marginLeft: '0'
                }
            }
        }
        
        const { title, drives, locationParam } = this.state
        console.log(this.props.drives);
        let bin = drives.filter(drive => drive.location === this.props.match.params.name && drive.yes >= 1);
        let ssd = drives.filter(bin => bin.driveType === 'SSD');
        let resultArr = [];
        let dateArr = []; 
        _.each(bin, function (bin) {
        // Easiest way to get your required date format
        console.log(bin.depositDate)
        let date = new Date(bin.depositDate).toISOString().replace(/T/, ' ').split(' ')[0]; 
        if (dateArr.indexOf(date) === -1) {
            dateArr.push(date);
            let obj = {Date: date, yes: bin.yes, price: bin.price};
            resultArr.push(obj);
        } 
        else {
            resultArr[dateArr.indexOf(date)].yes += bin.yes;
            resultArr[dateArr.indexOf(date)].price += bin.price;
        }
        });
        console.log(resultArr);
        return (
            <div>
                <AppAppBar currentRoute={ title } />
                <FilterBar onFilterChange={this.handleFilterChange} style={{marginBottom: 40}}/>
                <Grid style={{ paddingTop: 0, paddingRight: 0, paddingBottom: 0, paddingLeft: 0 }}>
                    <Row>
                        <Col width={6}>
                            <Card style={{...style.card.default}}>
                                <Row>
                                    <Col width={2} offset={1} style={{...style.cardLabel.siteInfo}} >
                                        <Icon name="server" style={{marginRight: 16, opacity: .75, color: 'white', display: 'block', margin: '100% auto', paddingLeft: '10px'}} />
                                    </Col>
                                    <Col width={9} style={{paddingLeft: '20px'}}>
                                        <SiteInfo current={locationParam}/>
                                    </Col>
                                </Row>
                            </Card>
                        </Col>
                        <Col width={6}>
                            <Card style={{...style.card.default}}>
                                <Row>
                                    {bin.length < 80 &&
                                    <Col width={2} offset={1} style={{...style.cardLabel.accessLite}} >
                                        <Icon name="trash" style={{marginRight: 16, opacity: .75, color: 'white', display: 'block', margin: '100% auto', paddingLeft: '10px'}} />
                                    </Col>
                                    }
                                    {bin.length > 80 &&
                                    <Col width={2} offset={1} style={{...style.cardLabel.accessFull}} >
                                        <Icon name="trash" style={{marginRight: 16, opacity: .75, color: 'white', display: 'block', margin: '100% auto', paddingLeft: '10px'}} />
                                    </Col>
                                    }
                                    <Col width={9}>
                                        <h3 style={{display: 'inline-block'}}>Bin 1</h3>
                                        <h6 style={{display: 'inline-block', float: 'right', marginTop: 30}}><b>Capacity:</b> {bin.length}/100</h6>
                                        <LineComparison total={bin.length} ssdCount={ssd.length} style={{display: 'block', clear: 'both'}} />
                                    </Col>
                                </Row>
                            </Card>
                        </Col>
                    </Row>
                    <Row style={{marginTop: 40}}>
                        <Col width={12}>
                            <Card style={{...style.card.default}}>
                                <Row style={{marginTop: 20}}>
                                    <Col width={12}>
                                        <h4 style={{textAlign: 'center'}}>Disposed Drives vs Total Price</h4>
                                    </Col>
                                </Row>
                                <ResponsiveContainer aspect={4.0/1.0} width='100%'>
                                    <LineChart data={resultArr}>
                                        <Line type="monotone" dataKey="yes" stroke="#8884d8" activeDot={{r: 8}} />
                                        <Line type="monotone" dataKey="price" stroke="#F44336" activeDot={{r: 8}} />
                                        <CartesianGrid stroke="#ccc" />
                                        <XAxis dataKey="Date" />
                                        <YAxis allowDecimals={false} />
                                        <Tooltip/>
                                        <Legend />
                                    </LineChart>
                                </ResponsiveContainer>
                            </Card>
                        </Col>
                    </Row>
                    <Row style={{marginTop: 40, marginBottom: 40}}>
                        {this.renderDrives()}
                    </Row>
                </Grid>
                {this.renderAdd()}
            </div>
        );
    }

}

const mapStateToProps = ({ drives, auth, user, props }) => ({ drives, auth, user, props });

export default withRouter(connect(mapStateToProps, { fetchDriveLocations, deleteDrive, fetchUser })(DriveList));
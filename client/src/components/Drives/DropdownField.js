// DriveField contains logic to render a single label and text input
import React from 'react';

export default ({ input, label, meta: { error, touched } }) => {
    return (
        <div className="input-field">
            <i className="material-icons prefix">gavel</i>
            <input {...input} style={{ marginBottom: '5px' }} id="autocomplete-input" className="dropdown"/>
            <label>{label}</label>
            <div className="red-text" style={{ marginBottom: '20px' }}>
                {touched && error}
            </div>
        </div>
    )
}
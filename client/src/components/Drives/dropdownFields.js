export default [
    { label: 'Drive Manufacturer', 
    name: 'manufacturer', 
    noValueError: 'You must specify a Drive Manufacturer',
    options: [
        { key: 'Dell', value: 'Dell'  },
        { key: 'HP', value: 'HP'  },
        { key: 'Cisco', value: 'Cisco'  },
        { key: 'Toshiba', value: 'Toshiba'  },
        { key: 'Seagate', value: 'Seagate'  },
        { key: 'HGST', value: 'HGST'  },
        { key: 'Intel', value: 'Intel'  }
    ] }
];
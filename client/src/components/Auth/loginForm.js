import React, { Component } from 'react'
import { reduxForm, Field } from 'redux-form'
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {TextField} from 'redux-form-material-ui'

class LoginForm extends Component {
  
  renderAlert() {
    if (this.props.errorMessage) {
      return <div><b>Oops:</b> {this.props.errorMessage}</div>
    }
  }

  render() {
    const {handleSubmit} = this.props

    return (
      <MuiThemeProvider muiTheme={getMuiTheme()}>
        <div>
        {this.renderAlert()}
        <form onSubmit={handleSubmit}>

          <Field
            label="Username"
            name="username"
            component={TextField}
            type="text"/>

          <Field
            label="Password"
            name="password"
            component={TextField}
            type="password"/>

          <button type="submit">Submit</button>
        </form>
        </div>
      </MuiThemeProvider>
    )
  }
}

export default reduxForm({
  form: 'login'
})(LoginForm)
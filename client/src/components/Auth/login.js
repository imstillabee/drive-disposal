import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom'
import * as actions from '../../actions';
import LoginForm from './loginForm'
class Login extends Component {

  componentWillUnmount() {
    if (this.props.errorMessage) {
      this.props.authError(null)
    }
  }

  displayRedirectMessages() {
    const location = this.props.location
    return location.state && <div>{location.state.message}</div>
  }


  handleSubmit({ username, password}) {
    console.log(username, password);
    //Need to do something to log user in
    this.props.loginUser({ username, password })
  }

  getRedirectPath() {
    const locationState = this.props.location.state
    if (locationState && locationState.from.pathname) {
      return locationState.from.pathname // redirects to reffering url
    } else {
      return '/dd/sites'
    }
  }

   render() {
    return (this.props.authenticated) ?
      <Redirect to={{
        pathname: this.getRedirectPath(), state: {
          from: this.props.location
        }
      }}/>
      :
      <div>
        {this.displayRedirectMessages()}
        <LoginForm onSubmit={this.handleSubmit.bind(this)} errorMessage={this.props.errorMessage}/>
      </div>
  }
}

function mapStateToProps(state) {
  return {
    authenticated: state.auth.authenticated,
    errorMessage: state.auth.error
  }
}

export default connect(mapStateToProps, actions)(Login)


import React, { Component } from 'react';
import PropTypes from 'prop-types'
import { connect } from 'react-redux';
import { fetchSiteLocation } from '../../actions';
import { withRouter } from 'react-router';
import moment from 'moment-timezone';
import timezone from '../../utils/timezone';

class SiteInfo extends Component {

    static propTypes = {
        match: PropTypes.object.isRequired,
        location: PropTypes.object.isRequired,
        history: PropTypes.object.isRequired
    }
    
 constructor(props) {
     super(props)
     this.state = {
         sites: this.props.sites || []
     }
 }

 componentWillMount() {
      this.props.fetchSiteLocation(this.props.match.params.name).then(() => {
            this.setState({sites: this.props.sites});
        })
 }


    renderTime(time) {
      const country = timezone(time.state);
      if ((time.region === 'US') || (time.region === 'North America') || (time.region === 'South America')) {
         return moment().tz(`America/` + country).format('hh:MM A')
      } else {
        return moment().tz(`${time.region}/` + country).format('hh:MM A')
      }
  }
  

    renderSites() {
        return this.state.sites.map(site => {
            return (
                <div key={site._id}>
                    <h4 style={{marginTop: 20, marginBottom: 0}}>{site.name} Info</h4>
                    <label style={{display: 'block'}}><b>Personnel On-Site:</b> {site.personnel}</label>
                    <label style={{display: 'block'}}><b>Email:</b> {site.email}</label>
                    <label style={{display: 'block'}}><b>Phone:</b> {site.phone}</label>
                    <label style={{display: 'block'}}><b>Mobile:</b> {site.mobile}</label>
                    <label style={{display: 'block'}}><b>Operation Hours:</b> {site.hours}</label>
                    <label style={{display: 'block'}}><b>Current Local Time:</b> {this.renderTime(site)}</label>
                </div>
            )
        })
    }

    render() {
        return (
            <div>
            {this.renderSites()}
            </div>
        )
    }

}

const mapStateToProps = ({ sites }) => ({ sites });

export default withRouter(connect(mapStateToProps, { fetchSiteLocation })(SiteInfo));
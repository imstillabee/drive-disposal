// DriveNew shows DriveForm and FormReview
import React, { Component } from 'react';
import { reduxForm } from 'redux-form';
import SiteForm from './SiteForm';
import SiteFormReview from './SiteFormReview';

class AddSite extends Component {

    state = { showSiteReview: false };

    renderContent() {
        if (this.state.showSiteReview) {
            return <SiteFormReview 
                onCancel={() => this.setState({showSiteReview: false })}    
            />;
        }

        return (
            <SiteForm 
                onSiteSubmit={() => this.setState({ showSiteReview: true})}
            />
        );
    }
    render() {
        return (
            <div>
                {this.renderContent()}
            </div>
        )
    }
}

export default reduxForm({
    form: 'siteForm'
})(AddSite);
// DriveForm shows a form for a user to add input
import _ from 'lodash';
import React, { Component } from 'react';
import { reduxForm, Field } from 'redux-form';
import { Link } from 'react-router-dom';
import SiteField from './SiteField';
import RegionField from './RegionField';
import siteFields from './siteFields';
import regionFields from './regionFields';
import validateEmails from '../../utils/validateEmails';

class SiteForm extends Component {
    renderInputs() {
        return _.map(siteFields, ({ label, name }) => {
            return (
                <Field 
                    key={name} 
                    component={SiteField} 
                    type="text" 
                    label={label} 
                    name={name} 
                /> 
            );
        });
    }

    renderSelect() {
        return _.map(regionFields, ({ label, name }) => {
            return (
                <Field 
                    key={name} 
                    component={RegionField}
                    type="select" 
                    label={label} 
                    name={name} 
                />
            );
        });
    }
    
    render() {
        return (
            <div className="container">
                <form onSubmit={this.props.handleSubmit(this.props.onSiteSubmit)}> {/*only call after user has submitted, not on render*/}
                    <h2 className="red-text darken-3">Add a New Cloud Site</h2>
                    {this.renderInputs()}
                    {this.renderSelect()}
                    <Link to="/" className="red btn-flat white-text">
                        Cancel
                    </Link>
                    <button type="submit" className="teal btn-flat right white-text">NEXT <i className="material-icons right">done</i></button>
                </form>
            </div>
        );
    }
}

function validate(values) {

    const errors = {};

    errors.email = validateEmails(values.email || '');

    _.each(siteFields, regionFields, ({ name, noValueError }) => {
        if (!values[name]) {
            errors[name] = noValueError;
        }
    });

    return errors;
}

    

export default reduxForm({
    validate,
    form: 'siteForm',
    destroyOnUnmount: false //keep form values
})(SiteForm);
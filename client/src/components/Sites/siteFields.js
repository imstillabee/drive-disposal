export default [
    { label: 'City', name: 'name', noValueError: 'You must provide a Site City' },
    { label: 'Name of Personnal on site', name: 'personnel', noValueError: 'You must provide a personel name' },
    { label: 'Phone Number', name: 'phone', noValueError: 'You must provide a main phone number' },
    { label: 'Mobile Number', name: 'mobile', noValueError: 'You must provide a mobile phone number' },
    { label: 'Hours of Operation', name: 'hours', noValueError: 'You must provide the hours of operation' },
    { label: 'Personnel Email', name: 'email', noValueError: 'You must provide the email of the personnel contact' }
];
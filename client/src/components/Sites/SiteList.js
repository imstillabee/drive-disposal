import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { fetchSites, deleteSite, fetchUser } from '../../actions';
import { Link } from 'react-router-dom';
import moment from 'moment-timezone';
import { Grid, Row, Col } from '_forge/Grid'
import Card from '_forge/Card';
import Progress from '_forge/Progress'
import Button from '_forge/Button';
import AppAppBar from '_forge/AppAppBar'
import Transition from 'react-transition-group/Transition'
import Icon from '_forge/Icon'
import FuzzySearch from 'fuzzy-search'
import FilterBar from '_forge/FilterBar'
import timezone from '../../utils/timezone';
import cookie from 'react-cookie';

class SiteList extends Component {

    static propTypes = {
        match: PropTypes.object.isRequired,
        location: PropTypes.object.isRequired,
        history: PropTypes.object.isRequired
    }

    constructor(props) {
        super(props)
        this.state = {
            single: '',
            title: 'Sites',
            sites: this.props.sites || [],
            filterQuery: ''
        }
    }

    isRole(roleToCheck, toRender) {
        const userRole = this.props.user.profile.locations[0].city;

        if (userRole === roleToCheck) {
            return toRender;
        }

        return false;
    }

    componentWillMount() {
        this.props.fetchUser();
        
    }

    componentDidMount() {
        this.props.fetchSites().then(() => {
            this.setState({sites: this.props.sites}, this.handleFilterChange(this.state.filterQuery));
        });
        setTimeout(function() { this.setCity(); }.bind(this), 6000);
        setTimeout(function() { this.renderAdd(); }.bind(this), 6000);
    }

  renderDelete() {
    switch (this.props.auth) {
      case null:
        return;
      case false:
        return;
      default:
        return <button className="waves-effect waves-light btn white red-text">Delete</button>
    }
  }

      setCity() {
          const userRole = this.props.user.profile.locations[0].city;
      if (userRole !== 'All') {
        this.setState((state) => ({
            single: userRole
        }))
      } else {
          this.setState((state) => ({
            single: 'All'
        }))
      }
  }

  renderAdd() {
        if ((this.props.user.profile.role === 'Admin') || (this.props.user.profile.role === 'Developer') || (this.props.user.profile.locations[0].city === 'All')) {
            return <Link to="/dd/addsite"><Button size='lg' type='primary' submit style={{ width: '50%', display: 'block', margin: '0 auto 20px'}}>Add Site</Button></Link>
      } else {
        return;
      }
  }

  renderTime(time) {
      const country = timezone(time.state);
      if ((time.region === 'US') || (time.region === 'North America') || (time.region === 'South America')) {
         return moment().tz(`America/` + country).format('hh:MM A')
      } else {
        return moment().tz(`${time.region}/` + country).format('hh:MM A')
      }
  }

  renderView(place) {
        return (
        <Link to={place} style={{color: '#5C5C5C', textDecoration: 'none', fontSize: '14px', marginTop: '10px', marginBottom: '20px', display: 'block'}} className="waves-effect waves-light btn red darken-4 right">Click here to view</Link>
        )
  }

  renderAccess(site, style) {
      let driveCount = this.props.drives.length

      if ((this.state.single === site.name) || (this.state.single === 'All')) {
          if (driveCount <= 75) {
              return (
                <Col width={2} offset={1} style={{...style.cardLabel.accessLite}} >
                    <Icon name="server" style={{marginRight: 16, opacity: .75, color:'#222222', display: 'block', margin: '100% auto', paddingLeft: '10px'}} />
                </Col>
            )
          } else {
            return (
                <Col width={2} offset={1} style={{...style.cardLabel.accessFull}} >
                    <Icon name="server" style={{marginRight: 16, opacity: .75, color: '#B51C10', display: 'block', margin: '100% auto', paddingLeft: '10px'}} />
                </Col>
            )
          }
      } else {
        return (
        <Col width={2} offset={1} style={{...style.cardLabel.default}} >
            <Icon name="server" style={{marginRight: 16, opacity: .75, color: '#016726', display: 'block', margin: '100% auto', paddingLeft: '10px'}} />
        </Col>
        )
      }
  }

  handleFilterChange = (query) => {
    const searcher = new FuzzySearch(this.props.sites, ['name', 'region', 'personnel'])
    if(query.length) {
      this.setState({ sites: searcher.search(query), filterQuery: query })
    } else (
      this.setState({ sites: this.props.sites, filterQuery: '' })
    )
  }



    renderSites() {
        console.log(this.props.user)
        console.log(this.state.sites);
        const style = {
        card: {
            'default': {
                position: 'relative',
                boxSizing: 'border-box',
                overflow: 'hidden',
                marginBottom: '30px'
                }
            },
        cardLabel: {
            'default': {
                position:'relative',
                boxSizing: 'border-box',
                width:'10%',
                display: 'inline-block',
                float: 'left',
                background: '#ADADAD',
                marginLeft: '0'
            },
            'accessFull' : {
                position:'relative',
                boxSizing: 'border-box',
                width:'10%',
                display: 'inline-block',
                float: 'left',
                background: '#D52B1E',
                marginLeft: '0'
            },
            'accessLite' : {
                position:'relative',
                boxSizing: 'border-box',
                width:'10%',
                display: 'inline-block',
                float: 'left',
                background: '#00AC3E',
                marginLeft: '0'
            },
        }
        }
        return this.state.sites.map(site => {
            let place = `/dd/drives/${site.name}`
            return (
                <Col width={3} key={site._id}>
                  <Card style={{...style.card.default}}>
                      <Row>
                        {this.renderAccess(site, style)}
                    <Col width={9} style={{paddingLeft: '20px'}}>
                        <h4 style={{marginTop: 20, marginBottom: 0}}>{site.name}</h4>
                        <label style={{display: 'block'}}><Icon style={{verticalAlign: 'middle'}} name="user" /> <span style={{verticalAlign: 'bottom'}}>{site.personnel}</span></label>
                        <label style={{display: 'block'}}><Icon style={{verticalAlign: 'middle'}} name="clock" /> <span style={{verticalAlign: 'bottom'}}>{site.hours} - {this.renderTime(site)}</span></label>

                        {site.name === this.state.single ? this.renderView(place) : ''}
                        {this.state.single === 'All' ? this.renderView(place) : ''}
                    </Col>
                    </Row>
                  </Card>
                </Col>
                
            )
        })
    }

    render({props} = this) {
        const { title } = this.state
        console.log(this.props.user)
        return (
            <div>
                <AppAppBar currentRoute={ title } />
                <FilterBar onFilterChange={this.handleFilterChange} />
                <Grid style={{ paddingTop: 0, paddingRight: 0, paddingBottom: 0, paddingLeft: 0 }}>
                    <Row style={{marginTop: 40, marginBottom: 40}} >
                        {this.renderSites()}
                    </Row>
                </Grid>
                <div className="fixed-action-btn">
                    {this.state.single !== '' ? this.renderAdd() : ''}
                </div>
            </div>
        );
    }
}

const mapStateToProps = ({ sites, drives, auth, user, props }) => ({ sites, drives, auth, user, props });

export default connect(mapStateToProps, { fetchSites, deleteSite, fetchUser })(SiteList);
//DriveFromReview shows users thier form inputs for review
import _ from 'lodash';
import React from 'react';
import { connect } from 'react-redux';
import siteFields from './siteFields';
import regionFields from './regionFields';
import { withRouter } from 'react-router-dom';
import * as actions from '../../actions';

const SiteReview = ({ onCancel, siteValues, submitSite, history }) => {

    const reviewFields = _.map(siteFields, ({ name, label }) => {
        return (
            <div key={name}> {/* need unique key */}
                <label>{label}</label>
                <div>
                    {siteValues[name]}
                </div>
            </div>
        );
    });

    const reviewSelect = _.map(regionFields, ({ name, label }) => {
        return (
            <div key={name}> {/* need unique key */}
                <label>{label}</label>
                <div>
                    {siteValues[name]}
                </div>
            </div>
        );
    });

    return (
        <div className="container">
            <h2 className="red-text darken-3">Please confirm your entries</h2>
            {reviewFields}
            {reviewSelect}
            <button className="yellow darken-3 white-text btn-flat" onClick={onCancel}>
                Back
            </button>
            <button onClick={() => submitSite(siteValues, history)} className="green white-text btn-flat right">
                Submit Site
                <i className="material-icons right">email</i>
            </button>
        </div>
    );
};

function mapStateToProps(state) {
    // console.log(state);
    return { siteValues: state.form.siteForm.values };
}

export default connect(mapStateToProps, actions)(withRouter(SiteReview));
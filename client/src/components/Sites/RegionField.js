// DriveField contains logic to render a single label and text input
import React from 'react';

export default ({ input, label, meta: { error, touched } }) => {
    return (
        <div className="input-field">
            <i className="material-icons prefix">vpn_lock</i>
            <input {...input} style={{ marginBottom: '5px' }} id="autocomplete-select" className="regions"/>
            <label>{label}</label>
            <div className="red-text" style={{ marginBottom: '20px' }}>
                {touched && error}
            </div>
        </div>
    )
}